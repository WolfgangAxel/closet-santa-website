from django.test import TestCase, Client
from .models import Memorial, Quote
from subs.models import subreddit, series
from users.models import User


class ViewTest(TestCase):

    def setUp(self):
        self.c = Client()
        ser = series.objects.create(name="ser")
        subreddit.objects.create(title="oh_wow", related_series=ser)
        subreddit.objects.create(title="much_sub", related_series=ser)
        self.fall00 = Memorial.objects.create(year=2000,
                                              season="Autumn",
                                              total_participants=45,
                                              nice=43,
                                              naughty=2,
                                              gifts_delivered=45,
                                              messages_sent=395,
                                              visible=True)
        winter00 = Memorial.objects.create(year=2000,
                                           season="Winter",
                                           total_participants=40,
                                           nice=40,
                                           naughty=0,
                                           gifts_delivered=40,
                                           messages_sent=222,
                                           visible=False)
        summer07 = Memorial.objects.create(year=2007,
                                           season="Summer",
                                           total_participants=99,
                                           nice=90,
                                           naughty=9,
                                           gifts_delivered=99,
                                           messages_sent=1586,
                                           visible=True)
        mod = User.objects.create(username="moderator")
        reg = User.objects.create(username="regifter")
        spt = User.objects.create(username="special")
        Quote.objects.create(user=mod,
                             honor="MD",
                             yearbook=self.fall00,
                             message="Toosty testy tahst")
        Quote.objects.create(user=reg,
                             honor="RG",
                             yearbook=self.fall00,
                             message="Blah bleh bloop")
        Quote.objects.create(user=spt,
                             honor="ST",
                             yearbook=self.fall00,
                             message="Murph monph meph")

    def test_1_list_works(self):
        resp = self.c.get("/yearbook/")
        self.assertIn("historicals/yearbook_list.html",
                      [t.name for t in resp.templates])
        self.assertEqual(len(resp.context["years"]), 2)
        self.assertIn(b"Autumn 2000", resp.content)
        self.assertIn(b"Summer 2007", resp.content)

    def test_2_year_list_works(self):
        Memorial.objects.update(visible=True)
        resp = self.c.get("/yearbook/2000")
        self.assertIn("historicals/yearbook_list.html",
                      [t.name for t in resp.templates])
        self.assertEqual(len(resp.context["years"]), 2)
        self.assertIn(b"Autumn 2000", resp.content)
        self.assertIn(b"Winter 2000", resp.content)
        self.assertEqual(resp.content, self.c.get("/yearbook/2000/").content)

    def test_3_yearbook_loads(self):
        resp = self.c.get("/yearbook/2000/Autumn")
        self.assertIn("historicals/yearbook.html",
                      [t.name for t in resp.templates])
        self.assertEqual(self.fall00, resp.context["memorial"])
        self.assertIn(b"Toosty testy tahst", resp.content)
        self.assertIn(b"Blah bleh bloop", resp.content)
        self.assertIn(b"Murph monph meph", resp.content)

    def test_4_invisible_yearbook_fails(self):
        Memorial.objects.filter(year=2000,
                                season="Autumn").update(visible=False)
        resp = self.c.get("/yearbook/2000/Autumn")
        self.assertIn("error.html", [t.name for t in resp.templates])

    def test_5_invalid_yearbook_fails(self):
        resp = self.c.get("/yearbook/2048/Autumn")
        self.assertIn("error.html", [t.name for t in resp.templates])

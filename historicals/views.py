from django.shortcuts import render
from .models import Memorial
from admin_extras.views import task_notifs

# Create your views here.


@task_notifs
def collection(req):
    return render(
        req, 'historicals/yearbook_list.html',
        {"years": tuple(Memorial.objects.filter(visible=True).all())})


@task_notifs
def anthology(req, year):
    return render(req, 'historicals/yearbook_list.html', {
        "years":
        tuple(Memorial.objects.filter(visible=True, year=year).all())
    })


@task_notifs
def yearbook(req, year, season):
    try:
        mm = Memorial.objects.get(visible=True, year=year, season=season)
    except:
        context = {
            "title": "Yearbook does not exist",
            "message": "Yearbook does not exist",
            "reason": "There is no data recorded for this year."
        }
        return render(req, "error.html", context)
    context = {
        "memorial": mm,
        "m_quotes": tuple(mm.quote_set.filter(honor="MD").all()),
        "r_quotes": tuple(mm.quote_set.filter(honor="RG").all()),
        "s_quotes": tuple(mm.quote_set.filter(honor="ST").all()),
        "subs": [s for s in mm.subreddits.splitlines() if s != ""]
    }
    return render(req, 'historicals/yearbook.html', context)

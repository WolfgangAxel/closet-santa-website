from django.db import models
from time import gmtime
from users.models import User
from subs.models import subreddit

# For getting the season from gmtime().tm_mon
seasons = [
    None, "Winter", "Spring", "Spring", "Spring", "Summer", "Summer", "Summer",
    "Autumn", "Autumn", "Autumn", "Winter", "Winter"
]


def get_curent_year():
    return gmtime().tm_year


def get_subs():
    out = ""
    for sub in subreddit.objects.filter(participating=True).all():
        out += str(sub) + "\n"
    return out


class Memorial(models.Model):
    year = models.IntegerField(default=get_curent_year)
    season = models.CharField(max_length=6,
                              choices=(
                                  ("Winter", "Winter"),
                                  ("Spring", "Spring"),
                                  ("Summer", "Summer"),
                                  ("Autumn", "Autumn"),
                              ))
    total_participants = models.IntegerField(blank=True, null=True)
    nice = models.IntegerField(blank=True, null=True)
    naughty = models.IntegerField(blank=True, null=True)
    gifts_delivered = models.IntegerField(blank=True, null=True)
    messages_sent = models.IntegerField(blank=True, null=True)
    subreddits = models.TextField(default=get_subs)
    visible = models.BooleanField(default=False)

    def __str__(self):
        return self.season + " " + str(self.year)


class Quote(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    honor = models.CharField(max_length=2,
                             choices=(
                                 ("MD", "Moderator"),
                                 ("RG", "Regifter"),
                                 ("ST", "Special Thanks"),
                             ))
    yearbook = models.ForeignKey(Memorial, on_delete=models.CASCADE)
    message = models.TextField(null=True)

    def __str__(self):
        return self.honor + " | " + self.user.username

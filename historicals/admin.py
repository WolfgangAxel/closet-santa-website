from django.contrib import admin
from .models import Memorial, Quote

# Register your models here.
admin.site.register(Memorial)
admin.site.register(Quote)

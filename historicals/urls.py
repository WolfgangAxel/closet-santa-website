from django.urls import path
from . import views

urlpatterns = [
    path("", views.collection),
    path("<int:year>", views.anthology),
    path("<int:year>/", views.anthology),
    path("<int:year>/<season>", views.yearbook),
]

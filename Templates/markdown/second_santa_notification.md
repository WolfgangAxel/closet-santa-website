Hello again, /u/{{ user.username }}!

We are very sorry that your first Santa was unable to deliver a gift to you on time. We have paired you with a new Santa who had successfully delivered a gift during the first shipping phase, and have banned your prior Santa from participating next year. We apologize for the delay in you receiving your gift.

As before, you will need to [send your new Santa your key](https://www.reddit.com/message/compose/?to={{ m_bot }}&subject=To%20Santa-san) so that they will be able to view your shipping address.

If you have any questions or concerns, please [contact the moderators](https://www.reddit.com/message/compose/?to=/r/ClosetSanta) and someone will get back to you as soon as they can.

We thank you for your patience and understanding.

-- The Closet Santa Mod Team

^(This account inbox is unmonitored. If you need to contact us,) [^send ^us ^modmail](https://www.reddit.com/message/compose/?to=/r/ClosetSanta) ^instead

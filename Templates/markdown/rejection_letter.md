Hello /u/{{ user.username }},

We have reviewed your profile and unfortunately must inform you that your profile does not meet our [Elegibility Requirements](https://closetsanta.com/information#eleg). Here are our requirements and what we calculated your account to have:

* Account age
 * Minimum age in months: 18
 * Your account age: {{ age }}
* [Adjusted Karma](https://closetsanta.com/karma_adjust)
 * Minimum adjusted karma: 2000
 * Your adjusted karma: {{ user.karma }}

* On the **Naughty List**
 * Must be: False
 * Your result: {{ banned }}

We apologize for the inconvenience. These requirements are hard-set and necessary due to an unreasonably high volume of people who have leeched off of this exchange in the past. We hope you understand that we are merely trying to decrease the likelihood that leechers are allowed into the exchange, and that we must be strict in enforcing these requirements.

If you are *not* on the **Naughty List**, you can still be [sponsored](https://closetsanta.com/sponsorships/). Feel free to ask for a sponsorship in the comments of the exchange's stickied accouncement post in /r/ClosetSanta.

We hope that you come back and try again next year!

-- The Closet Santa Mod Team

^(This account inbox is unmonitored. If you need to contact us,) [^send ^us ^modmail](https://www.reddit.com/message/compose/?to=/r/ClosetSanta) ^instead

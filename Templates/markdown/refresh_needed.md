Hello /u/{{ user.username }}!

There was an issue when trying to access your account data to determine eligibility. Please log out of [the website](https://closetsanta.com/) and log back in to resolve this issue.

-- The Closet Santa Mod Team

^(This account inbox is unmonitored. If you need to contact us,) [^send ^us ^modmail](https://www.reddit.com/message/compose/?to=/r/ClosetSanta) ^instead

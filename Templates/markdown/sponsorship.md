Hello /u/{{ user.username }}!

We are happy to say that {{ sponsor.username }} has sponsored you for this year's Closet Santa exchange! This means that although you were previously notified that you were not accepted, you are now going to be participating in this exchange. Be aware that this means that if you end up on the **Naughty List**, so will your sponsor. Your sponsorship will end at the end of the first shipping phase.

Please be sure that you have reviewed the [Important Dates](https://closetsanta.com/information#dates) and are ready to ship your gift before the deadline. We also ask that you put aside money right now for your kouhai's gift so that you are sure to deliver a gift and avoid ending up on the **Naughty List**.

-- The Closet Santa Mod Team

^(This account inbox is unmonitored. If you need to contact us,) [^send ^us ^modmail](https://www.reddit.com/message/compose/?to=/r/ClosetSanta) ^instead

Hello again, /u/{{ user.username }}!

You indicated when signing up that you would be able to give a second gift to a Kouhai whose Santa failed to deliver one to them. Unfortunately, that has happened this year and we need your help. /u/{{ user.kouhai.username }} did not receive a gift, so you have been selected as their replacement Santa. It would be greatly appreciated if you would [send them a message](https://www.reddit.com/message/compose/?to={{ m_bot }}&subject=As%20Santa-san) to let them know that you will make sure they get something.

We thank you from the bottom of our hearts for helping make sure that everyone who participated has a happy holiday season.

-- The Closet Santa Mod Team

^(This account inbox is unmonitored. If you need to contact us,) [^send ^us ^modmail](https://www.reddit.com/message/compose/?to=/r/ClosetSanta) ^instead

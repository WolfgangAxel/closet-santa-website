Hello /u/{{ user.username }},

We regret to inform you that your sponsored user, /u/{{ bad_santa.username }} did not deliver a gift this year. As per [the rules you agreed to](https://closetsanta.com/sponsorships/) when you sponsored them, your account will be banned as well for allowing this user into the exchange when we would not have otherwise allowed them in. You will be allowed to participate again starting in {{ user.banned_until_year }}.

-- The Closet Santa Mod Team

^(This account inbox is unmonitored. If you need to contact us,) [^send ^us ^modmail](https://www.reddit.com/message/compose/?to=/r/ClosetSanta) ^instead

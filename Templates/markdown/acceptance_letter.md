Hello /u/{{ user.username }}!

We have reviewed your profile and are happy to say that you have been accepted into this year's Closet Santa gift exchange! Please be sure that you have reviewed the [Important Dates](https://closetsanta.com/information#dates) and are ready to ship your gift before the deadline. We also ask that you put aside money right now for your kouhai's gift so that you are sure to deliver a gift and avoid ending up on the **Naughty List**.

Your account was also given [three sponsorship invites](https://closetsanta.com/sponsorships/). You may use these to allow users who did not meet our account requirements to participate in the exchange. See the comments on the stickied exchange announcement on /r/ClosetSanta for users in need of sponsoring. *Use your sponsorships wisely*.

Thank you for signing up, and have a happy holiday season!

-- The Closet Santa Mod Team

^(This account inbox is unmonitored. If you need to contact us,) [^send ^us ^modmail](https://www.reddit.com/message/compose/?to=/r/ClosetSanta) ^instead

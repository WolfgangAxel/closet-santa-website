Hello /u/{{ user.username }}!


The first phase of shipping has begun: you will be giving a gift to /u/{{ user.kouhai.username }}! [Message them to say hello!](https://www.reddit.com/message/compose/?to={{ m_bot }}&subject=As%20Santa-san)

Please take this time to go to [our website](https://closetsanta.com) and make sure your shipping information is updated in your profile. Once you've verified that it's correct, [send your Santa your key](https://www.reddit.com/message/compose/?to={{ m_bot }}&subject=To%20Santa-san) so they can securely view your address information.

If you have any questions, please [contact the moderators](https://www.reddit.com/message/compose/?to=/r/ClosetSanta) and someone will get back to you as soon as they can.

Thank you for participating in the exchange, and have fun!

-- The Closet Santa Mod Team")

^(This account inbox is unmonitored. If you need to contact us,) [^send ^us ^modmail](https://www.reddit.com/message/compose/?to=/r/ClosetSanta) ^instead

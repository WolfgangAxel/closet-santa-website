from django.db import models

HOME_TEMPS = (
    ('home/homepage_no_signup.html', 'Signups not open'),
    ('home/homepage_signup.html', 'Signups open'),
    ('home/homepage_ship_1.html', 'First shipping phase'),
    ('home/homepage_ship_2.html', 'Second shipping phase'),
)


# Create your models here.
class PhaseSettings(models.Model):
    title = models.CharField(max_length=50)
    active = models.BooleanField(default=False)
    signups_allowed = models.BooleanField(default=False)
    proof_can_be_submitted = models.BooleanField(default=False)
    home_template = models.CharField(max_length=255,
                                     choices=HOME_TEMPS,
                                     null=False)

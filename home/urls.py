from django.urls import path, include
from . import views

urlpatterns = [
    path('', views.landing, name='Home'),
    path('information', views.info, name='Information'),
    path('security', views.security, name='Security'),
    path('contact', views.contact, name='Contact'),
    path('bounce', views.bounce),
    path('karma_adjust', views.karma_adj),
    path('theme_toggle', views.theme_toggle),
]

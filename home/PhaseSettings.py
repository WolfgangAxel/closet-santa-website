from .models import PhaseSettings


def initialize():
    _ = PhaseSettings.objects.get_or_create(
        title="No signups",
        active=True,
        signups_allowed=False,
        proof_can_be_submitted=False,
        home_template="home/homepage_no_signup.html")
    _ = PhaseSettings.objects.get_or_create(
        title="Signups",
        active=False,
        signups_allowed=True,
        proof_can_be_submitted=False,
        home_template="home/homepage_signup.html")
    _ = PhaseSettings.objects.get_or_create(
        title="First shipment",
        active=False,
        signups_allowed=False,
        proof_can_be_submitted=True,
        home_template="home/homepage_ship_1.html")
    _ = PhaseSettings.objects.get_or_create(
        title="Second shipment",
        active=False,
        signups_allowed=False,
        proof_can_be_submitted=True,
        home_template="home/homepage_ship_2.html")


def switch_to(phase_name):
    """
    Updates the given phase to be active and un-activates previous phases
    
    Valid phases:
    No signups
    Signups
    First shipment
    Second shipment
    """
    PhaseSettings.objects.filter(active=True).update(active=False)
    ps = PhaseSettings.objects.filter(title=phase_name).first()
    if ps:
        ps.active = True
        ps.save()
    elif PhaseSettings.objects.count() == 0:
        # This branch is probably for tests, where the Deploy job isn't run
        # Initialize and try again
        initialize()
        switch_to(phase_name)
    else:
        # Default to no signups if something is effed up
        switch_to("No signups")


def _get_trait(trait):
    ps = PhaseSettings.objects.filter(active=True).first()
    if ps:
        return getattr(ps, trait)
    else:
        switch_to("No signups")
        return _get_trait(trait)


def get_title():
    return _get_trait("title")


def get_home_template():
    return _get_trait("home_template")


def get_signups_allowed():
    return _get_trait("signups_allowed")


def get_proof_can_be_submitted():
    return _get_trait("proof_can_be_submitted")

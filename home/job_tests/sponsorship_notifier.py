from django.test import TestCase
from home.jobs.sponsorship_notifier import Job
from sponsorship.models import Invitation
from users.models import User


class SponsorshipNotifierTest(TestCase):

    def setUp(self):
        user1 = User.objects.create(username="user1")
        user2 = User.objects.create(username="user2")
        Invitation.objects.create(sponsor=user1, invitee=user2)
        self.job = Job().for_testing()

    def test_1_sends_message(self):
        self.job._call_count = {}
        self.job.execute()
        self.assertEqual(self.job._call_count.get("message", 0), 1)

    def test_2_marks_notified(self):
        self.job.execute()
        self.assertEqual(Invitation.objects.filter(notified=False).count(), 0)

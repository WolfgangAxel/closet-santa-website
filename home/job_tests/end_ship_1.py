from django.test import TestCase
from home import PhaseSettings
from home.jobs.end_ship_1 import Job
from admin_extras.models import Task


class EndShip1Test(TestCase):

    def setUp(self):
        PhaseSettings.initialize()
        self.job = Job().for_testing()

    def test_1_phase_changes_properly(self):
        PhaseSettings.switch_to("First Shipment")
        self.job.execute()
        self.assertEqual(PhaseSettings.get_title(), "No signups")

    def test_2_panic_notifies_admins(self):
        Task.objects.all().delete()
        self.job._call_count = {}
        self.job.execute()
        self.assertEqual(Task.objects.count(), 1)
        self.assertEqual(self.job._call_count.get("message"), 1)

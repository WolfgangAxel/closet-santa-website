from django.test import TestCase
from praw import Reddit
from warnings import simplefilter


class fake_requestor(object):
    """
    Override praw's http requestor with one that
    simply returns pre-defined data, allowing for
    praw to be tested without actually making calls
    to Reddit and/or exposing secrets in the test
    environment
    """
    _json = {}
    status_code = 200
    oauth_url = ""
    reddit_url = ""
    headers = {
        "x-ratelimit-reset": 999999,
        "x-ratelimit-remaining": 999999,
        "x-ratelimit-used": 0
    }

    def __init__(self):
        pass

    def load_json(self, json):
        """
        Used to set the next response data
        """
        self._json = json
        self._json["expires_in"] = 999999
        self._json["access_token"] = "abc123"  #nosec
        self._json["scope"] = "everything"

    def smuggle(self, *args, **kwargs):
        """
        Pass this into praw's Reddit class as the keyword
        argument as follows:
            fr = fake_requestor()
            Reddit(... requestor_class=fr.smuggle)
        """
        return self

    # Compatibility methods
    def close(self, *args, **kwargs):
        pass

    def json(self, *args, **kwargs):
        return self._json

    def request(self, *args, **kwargs):
        return self

    @property
    def content(self):
        return bytes(str(self._json), "utf-8")


class PrawTest(TestCase):
    """
    Tests to make sure that within praw:
        - All required properties and functions exist
        - Nothing is being depreciated
    Note that all of the supplied "response" data mimics
    the Reddit API to some degree. If the API ever changes
    in such a way that makes praw change its internals,
    all of these tests will fail and need to be rewritten.
    """

    def setUp(self):
        self.faker = fake_requestor()
        # Set all warnings to count as errors instead
        # This way we stay ahead of depreciation warnings and others
        simplefilter("error")
        self.R = Reddit(  #nosec
            username="fake_user_123",
            password="123456",
            client_id="xxxxxxxxx-xxxx",
            client_secret="xxxxxxxxxxxxxxxxxxxxxxxxxxx",
            user_agent="Hello faking!",
            requestor_class=self.faker.smuggle)
        self.faker.load_json({
            "name": "fake_user_123",
            "created_utc": 999999.0,
            "link_karma": 1,
            "comment_karma": 0
        })
        self.me = self.R.user.me()

    def tearDown(self):
        simplefilter("default")

    def test_1_reddit_user(self):
        _ = self.R.redditor("fake_user_123")
        _ = self.R.redditor("fake_user_123").message(subject="this should",
                                                     message="be fine")
        self.assertEqual(self.me.link_karma, 1)
        self.assertEqual(self.me.comment_karma, 0)
        self.assertEqual(self.me.created_utc, 999999.0)

    def test_2_listings(self):
        self.faker.load_json({"kind": "Listing", "data": {"children": []}})
        self.assertEqual(tuple(self.me.submissions.top(limit=25)), tuple())
        self.assertEqual(tuple(self.me.comments.top(limit=50)), tuple())
        self.assertEqual(tuple(self.me.upvoted(limit=100)), tuple())

    def test_3_content_properties(self):
        self.faker.load_json({
            "kind": "Listing",
            "data": {
                "after":
                "xxxxxx",
                "children": [{
                    "kind": "t3",
                    "data": {
                        "after": "xxxxxx",
                        "subreddit": "ClosetSanta",
                        "id": "xxxxxx",
                        "score": 2
                    },
                    "after": "xxxxxx"
                }]
            }
        })
        up = next(self.me.upvoted(limit=1))
        sm = next(self.me.submissions.top(limit=1))
        cm = next(self.me.comments.top(limit=1))
        self.assertEqual(up.subreddit.display_name.lower(), "closetsanta")
        self.assertEqual(sm.subreddit.display_name.lower(), "closetsanta")
        self.assertEqual(cm.subreddit.display_name.lower(), "closetsanta")
        self.assertEqual(sm.score, 2)
        self.assertEqual(cm.score, 2)

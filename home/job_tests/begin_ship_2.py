from django.test import TestCase
from home import PhaseSettings
from home.jobs.begin_ship_2 import Job
from users.models import User
from subs.models import subreddit, series
from api.models import Nonce
from time import gmtime


class BeginShip2Test(TestCase):

    def refresh(self):
        User.objects.filter(is_staff=True).update(kouhai=None,
                                                  verified_proof=True)
        self.job._call_count = {}
        Nonce.objects.all().delete()

    def set_needs_gifting(self, *args):
        """
        Pass in a list of numbers, set those users as needing regifting
        """
        for i in range(1, 11):
            User.objects.filter(username="user" + str(i)).update(
                accepted_into_exchange=(i in args))

    def set_regifters(self, *args):
        """
        Pass in a list of numbers, set those users as regifters
        """
        for i in range(1, 11):
            User.objects.filter(username="user" +
                                str(i)).update(regifter=(i in args))

    def set_banned(self, *args):
        """
        Pass in a list of numbers, set those users as banned
        """
        year = gmtime().tm_year
        for i in range(1, 11):
            User.objects.filter(username="user" + str(i)).update(
                banned_until_year=year + 2 * int(i in args))

    def setUp(self):
        PhaseSettings.initialize()
        self.job = Job().for_testing()
        ser = series.objects.create(name="ser")
        sub = subreddit.objects.create(title="subsub", related_series=ser)
        for i in range(1, 11):
            User.objects.create(username="user" + str(i),
                                location="US",
                                subreddit=sub,
                                international=False,
                                is_staff=True)

    def test_1_phase_set_properly(self):
        self.refresh()
        self.set_needs_gifting(1)
        self.set_regifters(2, 3, 4, 5, 6)
        self.job.execute()
        self.assertEqual("Second shipment", PhaseSettings.get_title())

    def test_2_nonce_generated(self):
        self.refresh()
        self.set_needs_gifting(1)
        self.set_regifters(2, 3, 4, 5, 6)
        self.assertEqual(0, Nonce.objects.count())
        self.job.execute()
        self.assertEqual(1, Nonce.objects.count())

    def test_3_only_use_needed_regifters(self):
        self.refresh()
        self.set_needs_gifting(1)
        self.set_regifters(2, 3, 4, 5, 6)
        self.job.execute()
        self.assertEqual(User.objects.filter(kouhai__isnull=False).count(), 1)

    def test_4_only_use_valid_regifters(self):
        self.refresh()
        self.set_needs_gifting(2, 4)
        self.set_regifters(2, 3, 4, 5, 6, 10)
        self.set_banned(6, 10)
        self.job.execute()
        """
        In this scenario, 2 and 4 need gifts, making them unsuitable regifters
        6 and 10 were banned, making them unsuitable regifters
        so, user3 and user5 should be the only two with kouhai
        """
        self.assertEqual(User.objects.filter(kouhai__isnull=False).count(), 2)
        self.assertIsNotNone(User.objects.get(username="user3").kouhai)
        self.assertIsNotNone(User.objects.get(username="user5").kouhai)

    def test_5_messages_send(self):
        self.refresh()
        self.set_needs_gifting(1, 2, 7)
        self.set_regifters(2, 3, 4, 5, 6)
        self.job.execute()
        # For each user that still needs a gift, there should be 2 messages
        # There is one extra message call for updating the bot listing
        self.assertEqual(self.job._call_count.get("message"), 3 * 2 + 1)

from django.test import TestCase
from django.contrib.auth.models import Group, Permission
from home import PhaseSettings
from home.jobs.deploy import Job, mod_perms
from django.conf import settings
from os.path import join
from tempfile import mkstemp
from re import search


class DeployTest(TestCase):
    """
    This job sets up the site after a fresh git clone.
    """

    def mock_update_settings_file(self, fp):
        pass

    def test_1_phases_initialize(self):
        job = Job()
        job.update_settings_file = self.mock_update_settings_file
        job.execute()
        self.assertEqual(PhaseSettings.get_title(), "No signups")

    def test_2_settings_file_overwritten_properly(self):
        _, fp = mkstemp()
        with open(fp, "w") as temp_file:
            temp_file.write(
                "#Totally real settings\n\nDEBUG = True\n\nSECRET_KEY = 'CHANGE_ME_BEFORE_YOU_LAUNCH'"
            )
        job = Job()
        job.update_settings_file(fp)
        with open(fp, "r") as temp_file:
            tf = temp_file.read()
            self.assertIsNotNone(search(r"DEBUG = False", tf))
            self.assertIsNotNone(
                search(
                    r"SECRET_KEY = '[abcdefghjkmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ23456789]{48}'",
                    tf))

    def test_3_groups_initialize(self):
        Group.objects.all().delete()
        job = Job()
        job.update_settings_file = self.mock_update_settings_file
        job.execute()
        kami = Group.objects.get(name="Kami-sama")
        mods = Group.objects.get(name="Mod-sama")
        self.assertIsNotNone(kami)
        self.assertIsNotNone(mods)
        for p in Permission.objects.all():
            self.assertTrue(
                kami.permissions.filter(codename=p.codename).exists())
            if p.name in mod_perms.get(p.content_type, []):
                self.assertTrue(
                    mods.permissions.filter(codename=p.codename).exists())

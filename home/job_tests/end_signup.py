from django.test import TestCase
from home import PhaseSettings
from home.jobs.end_signup import Job


class EndSignupTest(TestCase):
    """
    This job just moves the site back to the No Signup phase
    """

    def test_phases_set_properly(self):
        PhaseSettings.initialize()
        job = Job()
        job.execute()
        self.assertEqual(PhaseSettings.get_title(), "No signups")

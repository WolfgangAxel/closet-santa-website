from django.test import TestCase
from home.jobs.hourly.Karma_Validator import Job
from django.contrib.sites.models import Site
from allauth.socialaccount.models import SocialToken, SocialAccount
from users.models import User
from time import time
from datetime import datetime, timedelta


class pseudouser(object):
    """
    A stand-in for praw's user model.
    Because there's no way to alter the Reddit
    module inside the function to allow for testing.
    """
    _last_requested = ""
    _response_data = {
        "submissions": [],
        "comments": [],
        "upvoted": [],
    }

    def init(self, *args, **kwargs):
        return self

    @property
    def submissions(self):
        self._last_requested = "submissions"
        return self

    @property
    def comments(self):
        self._last_requested = "comments"
        return self

    def top(self, *args, **kwargs):
        return self._response_data[self._last_requested]

    def upvoted(self, *args, **kwargs):
        return self._response_data["upvoted"]


class pseudodata(object):
    """
    Mimics the structure of data returned from praw
    """

    def __init__(self, subname, **kwargs):
        self.sub = subname
        for key in kwargs:
            setattr(self, key, kwargs[key])

    @property
    def subreddit(self):
        return self

    @property
    def display_name(self):
        return self.sub


class KarmaValidatorTest(TestCase):

    def reset_account_data(self, data):
        self.job._call_count = {}
        self.job._call_order = []
        User.objects.update(checked_karma=False)
        SocialAccount.objects.all().delete()
        account = SocialAccount.objects.create(user=self.omnibli,
                                               provider="reddit",
                                               uid="omnipotence_is_bliss",
                                               extra_data=data)
        account.socialtoken_set.create(  # nosec
            app=self.socialapp,
            account=account,
            token="terkern",
            token_secret="ssshhhh",
            expires_at=datetime.utcnow().astimezone() + timedelta(days=1))

    def setUp(self):
        self.job = Job().for_testing()
        self.current_site = Site.objects.get_current()
        self.socialapp = self.current_site.socialapp_set.create(  # nosec
            provider="reddit",
            name="reddit",
            client_id="1234567890",
            secret="0987654321",
        )
        self.omnibli = User.objects.create(is_staff=True,
                                           completed_settings=True,
                                           username="omnipotence_is_bliss")

    def test_1_karma_calculates_properly(self):
        self.reset_account_data({
            "created_utc": 7,
            "link_karma": 12000,
            "comment_karma": 19000,
        })
        pu = pseudouser()
        pu._response_data = {
            "submissions": [
                pseudodata("askreddit", score=10000),
                pseudodata("manga", score=3333)
            ],
            "comments": [
                pseudodata("animemes", score=1590),
                pseudodata("creepy", score=1010)
            ],
            "upvoted": [pseudodata("manga")] * 10 + [pseudodata("news")] * 10,
        }
        self.job.get_reddit_user = pu.init
        self.job.execute()
        total = sum((
            12000,  # all link karma
            19000,  # all comment karma
            -10000,  # submission to restricted sub
            -1010,  # comment in restricted sub
            10  # upvotes in acceptable subs
        ))
        # the cached user object is stale, so refresh it
        self.omnibli.refresh_from_db()
        self.assertEqual(self.omnibli.karma, total)

    def test_2_proper_letter_sent(self):
        self.reset_account_data({
            "created_utc": 7,
            "link_karma": 12000,
            "comment_karma": 19000,
        })
        pu = pseudouser()
        pu._response_data = {
            "submissions": [
                pseudodata("askreddit", score=10000),
                pseudodata("manga", score=3333)
            ],
            "comments": [
                pseudodata("animemes", score=1590),
                pseudodata("creepy", score=1010)
            ],
            "upvoted": [pseudodata("manga")] * 10 + [pseudodata("news")] * 10,
        }
        self.job.get_reddit_user = pu.init
        self.job.execute()
        #                                      |> Last call made
        #                                      |  |> name of the function called
        self.assertEqual(self.job._call_order[-1][0], "message")
        #                         |> Last call made
        #                         |  |> The arguments passed in
        #                         |  |  |> *kwargs
        self.assertEqual(  #      |  |  |
            self.job._call_order[-1][1][1]["subject"],
            "You have been accepted into the Closet Santa Exchange!")
        #############################################################
        self.reset_account_data({
            "created_utc": time(),
            "link_karma": 1,
            "comment_karma": 0,
        })
        pu = pseudouser()
        self.job.get_reddit_user = pu.init
        self.job.execute()
        self.assertEqual(
            self.job._call_order[-1][1][1]["subject"],
            "You have not been accepted into the Closet Santa Exchange")

    def test_3_sponsorships_awarded(self):
        self.reset_account_data({
            "created_utc": 7,
            "link_karma": 12000,
            "comment_karma": 19000,
        })
        pu = pseudouser()
        pu._response_data = {
            "submissions": [],
            "comments": [],
            "upvoted": [],
        }
        self.job.get_reddit_user = pu.init
        self.job.execute()
        self.omnibli.refresh_from_db()
        self.assertEqual(self.omnibli.sponsorship_invites, 3)

    def test_4_expired_token_yields_action(self):
        self.reset_account_data({
            "created_utc": 7,
            "link_karma": 12000,
            "comment_karma": 19000,
        })
        pu = pseudouser()
        pu._response_data = {
            "submissions": [],
            "comments": [],
            "upvoted": [],
        }
        self.job.get_reddit_user = pu.init
        SocialAccount.objects.first().socialtoken_set.update(
            expires_at=datetime.utcnow().astimezone() - timedelta(days=1))
        self.job.execute()
        self.omnibli.refresh_from_db()
        self.assertEqual(self.job._call_count["message"], 1)
        self.assertTrue(self.omnibli.awaiting_token_refresh)

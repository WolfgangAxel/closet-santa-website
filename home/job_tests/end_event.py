from django.test import TestCase
from django.conf import settings
from users.models import User, Proof
from historicals.models import Memorial, seasons
from django.contrib.sites.models import Site
from allauth.socialaccount.models import SocialToken, SocialAccount
from admin_extras.models import Task
from home.jobs.end_event import Job
from subs.models import subreddit, series
from os import mkdir, listdir, remove, rmdir
from os.path import join, exists
from time import gmtime


class EndEventTest(TestCase):

    def setUp(self):
        self.job = Job().for_testing()

    def test_1_media_deleted(self):
        u = User.objects.create(username="user")
        p = Proof.objects.create(user=u)
        if not exists(settings.MEDIA_ROOT):
            mkdir(settings.MEDIA_ROOT)
        tempdir = join(settings.MEDIA_ROOT, "user_test_proof")
        mkdir(tempdir)
        for i in range(5):
            with open(join(tempdir, f"{i}-fake.jpg"), "wb") as f:
                f.write(b"garbage")
        self.job.execute()
        try:
            self.assertEqual(Proof.objects.count(), 0)
            self.assertFalse(exists(tempdir))
        except Exception as E:
            # Clean up before throwing the Exception
            for i in range(5):
                remove(join(tempdir, f"{i}-fake.jpg"))
            rmdir(tempdir)
            raise E

    def test_2_tasks_deleted(self):
        Task.objects.create(title="whatever", description="meh")
        self.job.execute()
        self.assertEqual(Task.objects.count(), 0)

    def test_3_santas_banned_and_whitelisted_properly(self):
        sad_user = User.objects.create(username="sad_user")
        bad_santa = User.objects.create(username="bad_santa",
                                        verified_proof=False,
                                        kouhai=sad_user)
        User.objects.create(username="good_santa",
                            verified_proof=True,
                            kouhai=bad_santa)
        self.job.execute()
        self.assertEqual(
            User.objects.get(username="bad_santa").banned_until_year,
            gmtime().tm_year + 2)
        self.assertEqual(
            User.objects.get(username="bad_santa").whitelist_until_year,
            gmtime().tm_year)
        self.assertEqual(
            User.objects.get(username="good_santa").banned_until_year,
            gmtime().tm_year)
        self.assertEqual(
            User.objects.get(username="good_santa").whitelist_until_year,
            gmtime().tm_year + 2)

    def test_4_users_reset(self):
        ser = series.objects.create(name="ser")
        subsub = subreddit.objects.create(title="subsub", related_series=ser)
        kouhai = User.objects.create(username="kouhai")
        User.objects.create(username="user",
                            location="US",
                            kouhai=kouhai,
                            regifter=True,
                            international=True,
                            subreddit=subsub,
                            completed_settings=True,
                            address_info="blarg",
                            done_submitting_proof=True,
                            verified_proof=True,
                            age=7777,
                            karma=7777,
                            checked_karma=True,
                            banned_until_year=2021,
                            whitelist_until_year=2021,
                            accepted_into_exchange=True)
        self.job.execute()
        u = User.objects.get(username="user")
        self.assertEqual(u.location, None)
        self.assertEqual(u.kouhai, None)
        self.assertEqual(u.regifter, False)
        self.assertEqual(u.international, False)
        self.assertEqual(u.subreddit, None)
        self.assertEqual(u.completed_settings, False)
        self.assertEqual(u.address_info, "")
        self.assertEqual(u.done_submitting_proof, False)
        self.assertEqual(u.verified_proof, False)
        self.assertEqual(u.age, None)
        self.assertEqual(u.karma, None)
        self.assertEqual(u.checked_karma, False)
        self.assertEqual(u.accepted_into_exchange, False)

    def test_5_admin_accounts_remain_active(self):
        User.objects.create(username="average_joe",
                            is_staff=False,
                            is_active=True)
        User.objects.create(username="admin", is_staff=True, is_active=True)
        self.job.execute()
        self.assertFalse(User.objects.get(username="average_joe").is_active)
        self.assertTrue(User.objects.get(username="admin").is_active)

    def test_6_anon_user_remains_active(self):
        User.objects.create(username="average_joe",
                            is_staff=False,
                            is_active=True)
        self.job.execute()
        self.assertFalse(User.objects.get(username="average_joe").is_active)
        self.assertTrue(User.objects.get(username="AnonymousUser").is_active)

    def test_7_socialtokens_are_deleted(self):
        current_site = Site.objects.get_current()
        socialapp = current_site.socialapp_set.create(  # nosec
            provider="reddit",
            name="reddit",
            client_id="1234567890",
            secret="0987654321",
        )
        omnibli = User.objects.create(is_staff=True,
                                      username="omnipotence_is_bliss")
        account = SocialAccount.objects.create(user=omnibli,
                                               provider="reddit",
                                               uid="omnipotence_is_bliss",
                                               extra_data={"created_utc": 7})
        SocialToken.objects.create(  # nosec
            app=socialapp,
            account=account,
            token="terkern",
            token_secret="ssshhhh")
        # So much overhead just to make a single token to test with...
        self.job.execute()
        self.assertEqual(SocialToken.objects.count(), 0)

    def test_8_subreddits_hidden(self):
        ser = series.objects.create(name="ser")
        subreddit.objects.create(title="subsub",
                                 related_series=ser,
                                 participating=True)
        self.job.execute()
        self.assertEqual(
            subreddit.objects.filter(participating=True).count(), 0)

    def test_9_memorial_shown(self):
        now = gmtime()
        Memorial.objects.create(year=now.tm_year,
                                season=seasons[now.tm_mon],
                                total_participants=15,
                                gifts_delivered=13,
                                nice=13,
                                naughty=2)
        self.job.execute()
        self.assertTrue(
            Memorial.objects.get(year=now.tm_year,
                                 season=seasons[now.tm_mon]).visible)

from django.test import TestCase
from home import PhaseSettings
from home.jobs.begin_signup import Job
from users.models import User
from historicals.models import Memorial


class BeginSignupTest(TestCase):
    """
    This job should enable all accounts and change the current Phase
    """

    def setUp(self):
        PhaseSettings.initialize()
        self.job = Job().for_testing()

    def test_1_phase_switched_correctly(self):
        self.job.execute()
        self.assertEqual(PhaseSettings.get_title(), "Signups")

    def test_2_users_are_reactivated(self):
        for i in range(10):
            User.objects.create(
                username="user" + str(i + 1),
                is_active=False,
                # We get the anon user otherwise and this was my first idea to avoid that
                is_staff=True)
        self.job.execute()
        for (boolean, count) in ((True, 10), (False, 0)):
            self.assertEqual(
                User.objects.filter(is_active=boolean, is_staff=True).count(),
                count)

    def test_3_memorial_made(self):
        Memorial.objects.all().delete()
        self.job.execute()
        self.assertEqual(Memorial.objects.count(), 1)

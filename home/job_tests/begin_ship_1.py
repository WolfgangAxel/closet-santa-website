from django.test import TestCase
from home import PhaseSettings
from home.jobs.begin_ship_1 import Job
from users.models import User
from subs.models import subreddit, series
from api.models import Nonce
from historicals.models import Memorial, seasons
from time import gmtime


class BeginShip1Test(TestCase):

    def refresh(self):
        User.objects.filter(is_staff=True).update(kouhai=None,
                                                  accepted_into_exchange=True)
        self.job._call_count = {}
        Nonce.objects.all().delete()

    def setUp(self):
        PhaseSettings.initialize()
        self.job = Job().for_testing()
        ser = series.objects.create(name="ser")
        sub = subreddit.objects.create(title="subsub", related_series=ser)
        for i in range(10):
            User.objects.create(username="user" + str(i + 1),
                                location="US",
                                subreddit=sub,
                                international=False,
                                is_staff=True)

    def test_1_phase_set_properly(self):
        self.refresh()
        self.job.execute()
        self.assertEqual("First shipment", PhaseSettings.get_title())

    def test_2_allowed_users_only(self):
        self.refresh()
        User.objects.filter(username__in=("user1", "user3", "user5",
                                          "user7")).update(
                                              accepted_into_exchange=False)
        self.job.execute()
        # We can check the job's call count data to see how many times `message` was ran
        # It should be n+1, where n is the number of accepted users (because of notify_bot)
        self.assertEqual(6 + 1, self.job._call_count.get("message"))

    def test_3_nonce_generated(self):
        self.refresh()
        self.assertEqual(0, Nonce.objects.count())
        self.job.execute()
        self.assertEqual(1, Nonce.objects.count())

    def test_4_memorial_updated(self):
        now = gmtime()
        Memorial.objects.create(year=now.tm_year, season=seasons[now.tm_mon])
        self.refresh()
        self.job.execute()
        self.assertEqual(
            Memorial.objects.get(
                year=now.tm_year,
                season=seasons[now.tm_mon]).total_participants, 10)

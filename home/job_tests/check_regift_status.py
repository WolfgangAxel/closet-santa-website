from django.test import TestCase
from users.models import User
from subs.models import subreddit, series
from home.jobs.check_regift_status import Job
from home.jobs.cost_calculator import assignment
from historicals.models import Memorial, seasons
from time import gmtime
from sponsorship.models import Invitation


class CRGSTest(TestCase):
    """
    This job checks the need for regifters in between the first and second shipping phases.
    Specifically, it looks to make sure there are more regifters than people who didn't get
    a gift. If not, it notifies the admins to take action.
    """

    def refresh(self):
        User.objects.update(kouhai=None,
                            verified_proof=False,
                            regifter=False,
                            whitelist_until_year=gmtime().tm_year,
                            banned_until_year=gmtime().tm_year)
        ul = User.objects.filter(accepted_into_exchange=True).all()
        assignment(ul, ul)
        self.job._call_count = {}
        self.job._call_order = []

    def setUp(self):
        self.job = Job().for_testing()
        ser = series.objects.create(name="ser")
        sub = subreddit.objects.create(title="subsub", related_series=ser)
        defaults = {
            "location": "US",
            "subreddit": sub,
            "international": False,
            "accepted_into_exchange": True,
        }
        for i in range(10):
            User.objects.create(username="user" + str(i + 1), **defaults)

    def test_1_no_error_when_everyone_plays_nice(self):
        self.refresh()
        User.objects.update(verified_proof=True)
        self.job.execute()
        self.assertIsNone(self.job._call_count.get("subreddit"))

    def test_2_no_error_when_regifters_are_available(self):
        self.refresh()
        User.objects.update(verified_proof=True, regifter=True)
        User.objects.filter(username="user3").update(verified_proof=False)
        self.job.execute()
        self.assertIsNone(self.job._call_count.get("subreddit"))

    def test_3_panic_when_not_enough_regifters(self):
        self.refresh()
        User.objects.update(verified_proof=True)
        User.objects.filter(username="user3").update(verified_proof=False)
        self.job.execute()
        self.assertIsNotNone(self.job._call_count.get("subreddit"))

    def test_4_white_and_black_lists_update(self):
        self.refresh()
        User.objects.update(verified_proof=True)
        User.objects.filter(username="user3").update(verified_proof=False)
        self.job.execute()
        user1 = User.objects.get(username="user1")
        user3 = User.objects.get(username="user3")
        self.assertEqual(user1.whitelist_until_year, gmtime().tm_year + 2)
        self.assertEqual(user1.banned_until_year, gmtime().tm_year)
        self.assertEqual(user3.whitelist_until_year, gmtime().tm_year)
        self.assertEqual(user3.banned_until_year, gmtime().tm_year + 2)

    def test_5_memorial_updates(self):
        now = gmtime()
        Memorial.objects.create(year=now.tm_year, season=seasons[now.tm_mon])
        self.refresh()
        User.objects.update(verified_proof=True)
        self.job.execute()
        self.assertEqual(
            Memorial.objects.get(year=now.tm_year,
                                 season=seasons[now.tm_mon]).nice, 10)
        self.assertEqual(
            Memorial.objects.get(year=now.tm_year,
                                 season=seasons[now.tm_mon]).naughty, 0)
        self.assertEqual(
            Memorial.objects.get(year=now.tm_year,
                                 season=seasons[now.tm_mon]).gifts_delivered,
            10)

    def test_6_sponsorships_reset(self):
        self.refresh()
        User.objects.update(verified_proof=True)
        user1 = User.objects.get(username="user1")
        user3 = User.objects.get(username="user3")
        Invitation.objects.create(sponsor=user1, invitee=user3)
        user1.sponsorship_invites = 3
        user1.save()
        user3.is_sponsored = True
        user3.save()
        self.job.execute()
        self.assertEqual(Invitation.objects.count(), 0)
        user1.refresh_from_db()
        user3.refresh_from_db()
        self.assertEqual(user3.is_sponsored, False)
        self.assertEqual(user1.sponsorship_invites, 0)

    def test_7_sponsorship_punishment(self):
        self.refresh()
        user1 = User.objects.get(username="user1")
        user3 = User.objects.get(username="user3")
        Invitation.objects.create(sponsor=user1, invitee=user3)
        user3.is_sponsored = True
        user3.save()
        self.job.execute()
        now = gmtime()
        user1.refresh_from_db()
        user3.refresh_from_db()
        self.assertEqual(user1.banned_until_year, now.tm_year + 3)
        self.assertEqual(user3.banned_until_year, now.tm_year + 2)

    def test_8_sponsorship_message_sent(self):
        self.refresh()
        User.objects.update(verified_proof=True, regifter=True)
        user1 = User.objects.get(username="user1")
        user3 = User.objects.get(username="user3")
        Invitation.objects.create(sponsor=user1, invitee=user3)
        user3.is_sponsored = True
        user3.verified_proof = False
        user3.save()
        self.job.execute()
        self.assertEqual(self.job._call_count.get("message", 0), 1)

from django.test import TestCase
from home import PhaseSettings
from home.jobs.end_ship_2 import Job
from admin_extras.models import Task
from users.models import User
from historicals.models import Memorial, seasons
from time import gmtime


class EndShip2Test(TestCase):

    def setUp(self):
        PhaseSettings.initialize()
        self.job = Job().for_testing()

    def test_1_phase_changes_properly(self):
        PhaseSettings.switch_to("Second Shipment")
        self.job.execute()
        self.assertEqual(PhaseSettings.get_title(), "No signups")

    def test_2_users_without_gifts_notifies_admins(self):
        Task.objects.all().delete()
        self.job._call_count = {}
        sad_user = User.objects.create(username="sad_user", )
        User.objects.create(
            username="bad_santa",
            kouhai=sad_user,
            verified_proof=False,
        )
        self.job.execute()
        self.assertEqual(Task.objects.count(), 1)
        self.assertEqual(self.job._call_count.get("message"), 1)

    def test_3_memorial_updates(self):
        now = gmtime()
        Memorial.objects.create(year=now.tm_year,
                                season=seasons[now.tm_mon],
                                total_participants=15,
                                gifts_delivered=13,
                                nice=13,
                                naughty=2)
        User.objects.all().delete()
        sad_user = User.objects.create(username="sad_user", )
        User.objects.create(
            username="bad_santa",
            kouhai=sad_user,
            verified_proof=False,
        )
        happy_user = User.objects.create(username="happy_user", )
        User.objects.create(
            username="good_santa",
            kouhai=happy_user,
            verified_proof=True,
        )
        self.job.execute()
        m = Memorial.objects.get(year=now.tm_year, season=seasons[now.tm_mon])
        self.assertEqual(m.naughty, 3)
        self.assertEqual(m.nice, 12)
        self.assertEqual(m.gifts_delivered, 14)

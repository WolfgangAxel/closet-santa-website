from django.test import TestCase
from users.models import User
from subs.models import subreddit, series
from home.jobs.cost_calculator import assignment


class MatchingTest(TestCase):
    """
    Simulate matching to ensure desired pairs are generated
    """

    def make_user(self, userdict):
        u = User.objects.create(accepted_into_exchange=True, **userdict)
        setattr(self, userdict["username"], u)

    def execute(self):
        ul = User.objects.filter(accepted_into_exchange=True).all()
        assignment(ul, ul)
        for u in [a for a in dir(self) if a[:4] == "user"]:
            getattr(self, u).refresh_from_db()

    def setUp(self):
        self.ser1 = series.objects.create(name="Series One")
        self.ser2 = series.objects.create(name="Series Two")
        self.sub1 = subreddit.objects.create(title="sub1",
                                             related_series=self.ser1)
        self.sub2 = subreddit.objects.create(title="sub2",
                                             related_series=self.ser1)
        self.sub3 = subreddit.objects.create(title="sub3",
                                             related_series=self.ser2)

    def test_1_domestic_shipping_preferred(self):
        self.make_user({
            "username": "user1",
            "location": "US",
            "subreddit": self.sub1,
            "international": False
        })
        self.make_user({
            "username": "user2",
            "location": "US",
            "subreddit": self.sub2,
            "international": False
        })
        self.make_user({
            "username": "user3",
            "location": "GB",
            "subreddit": self.sub3,
            "international": False
        })
        self.make_user({
            "username": "user4",
            "location": "GB",
            "subreddit": self.sub2,
            "international": False
        })
        self.execute()
        self.assertEqual(self.user1.kouhai, self.user2)
        self.assertEqual(self.user3.kouhai, self.user4)

    def test_2_forced_international_in_EU_prefers_EU(self):
        self.make_user({
            "username": "user1",
            "location": "US",
            "subreddit": self.sub1,
            "international": False
        })
        self.make_user({
            "username": "user2",
            "location": "US",
            "subreddit": self.sub2,
            "international": False
        })
        self.make_user({
            "username": "user3",
            "location": "DE",
            "subreddit": self.sub3,
            "international": False
        })
        self.make_user({
            "username": "user4",
            "location": "FR",
            "subreddit": self.sub2,
            "international": False
        })
        self.execute()
        self.assertEqual(self.user1.kouhai, self.user2)
        self.assertEqual(self.user3.kouhai, self.user4)

    def test_3_inter_series_preferred(self):
        self.make_user({
            "username": "user1",
            "location": "US",
            "subreddit": self.sub1,  # Series One
            "international": False
        })
        self.make_user({
            "username": "user2",
            "location": "US",
            "subreddit": self.sub3,  # Series Two
            "international": False
        })
        self.make_user({
            "username": "user3",
            "location": "US",
            "subreddit": self.sub1,
            "international": False
        })
        self.make_user({
            "username": "user4",
            "location": "US",
            "subreddit": self.sub3,
            "international": False
        })
        self.execute()
        self.assertEqual(self.user1.kouhai, self.user2)
        self.assertEqual(self.user3.kouhai, self.user4)

    def test_4_inter_sub_preferred(self):
        self.make_user({
            "username": "user1",
            "location": "US",
            "subreddit": self.sub1,  # Series One
            "international": False
        })
        self.make_user({
            "username": "user2",
            "location": "US",
            "subreddit": self.sub2,  # Series One
            "international": False
        })
        self.make_user({
            "username": "user3",
            "location": "US",
            "subreddit": self.sub1,
            "international": False
        })
        self.make_user({
            "username": "user4",
            "location": "US",
            "subreddit": self.sub2,
            "international": False
        })
        self.execute()
        self.assertEqual(self.user1.kouhai, self.user2)
        self.assertEqual(self.user3.kouhai, self.user4)

    def test_5_volunteer_international_shippers_preferred_for_international(
            self):
        self.make_user({
            "username": "user1",
            "location": "US",
            "subreddit": self.sub1,
            "international": False
        })
        self.make_user({
            "username": "user2",
            "location": "US",
            "subreddit": self.sub3,
            "international": True
        })
        self.make_user({
            "username": "user3",
            "location": "FR",
            "subreddit": self.sub3,
            "international": False
        })
        self.execute()
        self.assertEqual(self.user1.kouhai, self.user2)
        self.assertEqual(self.user2.kouhai, self.user3)
        self.assertEqual(self.user3.kouhai, self.user1)

"""
Assign kouhais, move to first shipping phase
"""

from .phase_job_template import PhaseJob


class Job(PhaseJob):
    help = "Move site to first shipping phase"

    def execute(self):
        from users.models import User
        from home.jobs.cost_calculator import assignment
        from home import PhaseSettings
        from historicals.models import Memorial, seasons
        from time import gmtime

        PhaseSettings.switch_to("First shipment")

        ## Assign kouhai to users
        user_pool = tuple(
            User.objects.filter(accepted_into_exchange=True).all())
        # Get the new list of users as a text string,
        # save it, then notify the messaging bot
        listdata = assignment(user_pool, user_pool)
        self.notify_bot(listdata)
        self.notify_users(user_pool,
                          "You've been asigned a Kouhai and a Santa!",
                          "markdown/event_begin_notification.md")

        now = gmtime()
        Memorial.objects.filter(year=now.tm_year,
                                season=seasons[now.tm_mon]).update(
                                    total_participants=len(user_pool))

from django_extensions.management.jobs import BaseJob


class Job(BaseJob):
    help = "Dummy job"


class PhaseJob(BaseJob):
    help = "A job template that adds support for lazy Reddit calls"

    _reddit = None
    _call_count = {}
    _call_order = []

    def get_R(self):
        from home.jobs.secret import R_kwargs
        from praw import Reddit
        return Reddit(**R_kwargs)

    def for_testing(self):

        class ultimate_faker(object):

            def __getattribute__(obj, attr):
                if not self._call_count.get(attr):
                    self._call_count[attr] = 0
                self._call_count[attr] += 1
                self._call_order.append([attr, None])
                return obj

            def __call__(obj, *args, **kwargs):
                self._call_order[-1][1] = (args, kwargs)
                return obj

        def fake_get_R():
            return ultimate_faker()

        self.get_R = fake_get_R
        return self

    def notify_bot(self, listdata):
        """
        Sends a PM to the messaging bot with a nonce
        to retrieve the new list data
        """
        from home.jobs.secret import m_bot
        from api.models import Nonce
        n = Nonce.objects.create(listdata=listdata)
        self.get_R().redditor(m_bot).message(subject="newlist",
                                             message=str(n.uuid))

    def notify_users(self, user_pool, title, template, extra_context={}):
        """
        Sends messages to all users in the user pool
        
        Formats the body with m_bot and user
        """
        from django.template.loader import get_template
        from home.jobs.secret import m_bot
        for user in user_pool:
            self.get_R().redditor(user.username).message(
                subject=title,
                message=get_template(template).render(context={
                    "user": user,
                    "m_bot": m_bot,
                    **extra_context
                }))
        # Classes are weird with variable reuse
        extra_context = {}

    def notify_user(self, user, title, template, extra_context={}):
        """
        Notifies a single user
        Just shorthand, calls notify_users
        """
        self.notify_users((user, ),
                          title,
                          template,
                          extra_context=extra_context)
        # Classes are weird with variable reuse
        extra_context = {}

    def panic(self, title, body):
        """
        Handles sending notifications to both the admin tasks and modmail
        Mostly here so we can mock over the calls in testing
        """
        from admin_extras.models import Task
        Task.objects.create(title=title, description=body)
        self.get_R().subreddit("ClosetSanta").message(subject=title,
                                                      message=body)

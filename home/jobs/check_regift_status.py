"""
Check to make sure there are enough regifters

Execute this job halfway between the end of first shipment and the beginning of second shipment
to allow mods time to finish up proof submission review.
"""

from .phase_job_template import PhaseJob


class Job(PhaseJob):
    help = "Ensure there are enough gifters"

    def execute(self):
        from users.models import User
        from sponsorship.models import Invitation
        from historicals.models import Memorial, seasons
        from time import gmtime

        santas = {"bad": 0, "good": 0}
        now = gmtime()
        year = now.tm_year
        for user in User.objects.filter(accepted_into_exchange=True).all():
            if user.verified_proof:
                # Add this santa to the whitelist
                user.whitelist_until_year = max(year + 1,
                                                user.whitelist_until_year) + 1
                # Get the user's santa's shipping status
                # Santas are found in the "user_set" reference
                santa = user.user_set.first()
                if santa.verified_proof:
                    # Take this user out of the exchange
                    user.accepted_into_exchange = False
                santas["good"] += 1
            else:
                # Add this santa to the blacklist and take them out of the exchange
                user.banned_until_year = max(year + 1,
                                             user.banned_until_year) + 1
                user.accepted_into_exchange = False
                santas["bad"] += 1
            user.save()
        # Ban all users who sponsored a bad santa
        # This is done outside of the initial loop
        # Because updating a different user model
        # during the iteration causes a desync
        # between the database and the users in the
        # iterator
        for user in User.objects.filter(is_sponsored=True).all():
            sponsor = user.sponsorships.first().sponsor
            if user.banned_until_year >= year + 2:
                # We don't remove them from the exchange because
                # after the first loop, we know that they sent a
                # gift. It would be unfair for them to send a
                # gift, not get one in return, and get banned.
                # The ban is still in place though, since that
                # doesn't come in to play until next year's signups
                sponsor.banned_until_year = max(year + 1,
                                                user.banned_until_year) + 1
                self.notify_user(sponsor,
                                 "Your sponsored user failed to deliver",
                                 "markdown/sponsor_ban.md",
                                 extra_context={"bad_santa": user})
            else:
                sponsor.whitelist_until_year = max(
                    year + 1, user.whitelist_until_year) + 1
            sponsor.save()
        # Unset everyone's kouhai and remove all sponsorships
        User.objects.update(kouhai=None,
                            is_sponsored=False,
                            sponsorship_invites=0)
        Invitation.objects.all().delete()
        # Check to see that we have enough regifters
        # Anyone still left in the exchange needs a new santa
        needs_regifting = User.objects.filter(
            accepted_into_exchange=True).count()
        # Get regifters who weren't just banned
        # Also don't get regifters who didn't get a gift themselves, because that's just unfair
        regifters = User.objects.filter(regifter=True,
                                        verified_proof=True,
                                        accepted_into_exchange=False,
                                        banned_until_year__lte=year).count()
        if needs_regifting > regifters:
            # Panic!
            self.panic(
                "It's time to panic! There are not enough regifters!",
                "There are currently {0} users in need of regifting but only {1} "
                .format(needs_regifting, regifters) +
                "regifters available. We will need to make an announcement asking "
                + "for more regifters ASAP")

        Memorial.objects.filter(year=year, season=seasons[now.tm_mon]).update(
            nice=santas["good"],
            naughty=santas["bad"],
            gifts_delivered=santas["good"])

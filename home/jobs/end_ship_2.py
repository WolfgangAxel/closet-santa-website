"""
End the second shipping phase
"""

from .phase_job_template import PhaseJob


class Job(PhaseJob):
    help = "Move site out of second shipping phase"

    def execute(self):
        from users.models import User
        from home import PhaseSettings
        from historicals.models import Memorial, seasons
        from time import gmtime

        PhaseSettings.switch_to("No signups")

        # Anyone still left in the exchange did not get a gift D:
        bad_santas = tuple(
            User.objects.exclude(kouhai=None).filter(
                verified_proof=False).all())
        if bad_santas:
            # Panic!
            self.panic(
                "There are users who did not get a gift!",
                "The following users did not recieve a gift: " + ", ".join([
                    "<a href=/u/{0}>{0}</a>".format(u.kouhai)
                    for u in bad_santas
                ]) +
                ". Check to make sure there were no last-minute proof submissions by these user's Santas."
                +
                " If not, action should be taken to compensate these users in some way."
            )

        now = gmtime()
        search = Memorial.objects.filter(year=now.tm_year,
                                         season=seasons[now.tm_mon])
        if search.count() > 0:
            yearbook = search.first()
            yearbook.naughty += len(bad_santas)
            yearbook.nice -= len(bad_santas)
            yearbook.gifts_delivered = yearbook.total_participants - len(
                bad_santas)
            yearbook.save()

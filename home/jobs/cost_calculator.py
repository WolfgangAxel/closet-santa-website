"""
Implementation of http://csclab.murraystate.edu/%7Ebob.pilgrim/445/munkres.html

Calculate costs to assign kouhai to users
"""

EU = [
    'BE', 'FR', 'DE', 'IT', 'LU', 'NL', 'DK', 'IE', 'GR', 'PT', 'ES', 'AT',
    'FI', 'SE', 'CY', 'CZ', 'EE', 'HU', 'LV', 'LT', 'MT', 'PL', 'SK', 'SI',
    'BG', 'RO', 'HR'
]


def assignment(kouhais, santas):
    from munkres import Munkres
    santa_cost_matrix = []
    for santa in santas:
        costs = []
        for kouhai in kouhais:
            if santa == kouhai:
                costs.append(999)
                continue
            cost = 0
            if kouhai.location != santa.location:
                # Add a very slight cost to international shipping
                # to encourage domestic shipping where possible
                if santa.international:
                    cost += 1
                # Add a large cost for non-international users
                # in the EU to other users in the EU.
                elif santa.location in EU and kouhai.location in EU:
                    cost += 20
                # Add a very large cost to all other international scenarios
                else:
                    cost += 40
            if santa.subreddit.related_series == kouhai.subreddit.related_series:
                # Add moderate cost to same-series shipping
                # Same sub should take precedence over international
                cost += 10
                if santa.subreddit == kouhai.subreddit:
                    # Add more cost to dissuade same sub shipping
                    # Still should take precedence over international
                    cost += 5
            costs.append(cost)
        santa_cost_matrix.append(costs)
    m = Munkres()
    indices = m.compute(santa_cost_matrix)
    # New list sent to messaging bot so PMs are routed correctly
    # is the "to santa" table, so santa is key, kouhai is value
    newList = {}
    for s_ix, k_ix in indices:
        santas[s_ix].kouhai = kouhais[k_ix]
        # Reset the santa
        santas[s_ix].done_submitting_proof = False
        santas[s_ix].verified_proof = False
        santas[s_ix].total_uploaded = 0
        santas[s_ix].proof_set.all().delete()
        santas[s_ix].save()

        newList[kouhais[k_ix].username] = santas[s_ix].username
    from base64 import b64encode
    from pickle import dumps  # nosec
    return b64encode(dumps(newList)).decode()


from django_extensions.management.jobs import BaseJob


class Job(BaseJob):
    help = "Dummy job, this is a helper function"

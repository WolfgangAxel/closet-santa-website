"""
Daily backup job.
Makes a copy of the database somewhere else
"""

from django_extensions.management.jobs import DailyJob


class Job(DailyJob):
    help = "Daily database backups"

    def execute(self):
        from django.conf import settings
        if settings.DB_BACKUP_CMD != "":
            from subprocess import Popen  # nosec
            from shlex import split
            from os import name
            return Popen(
                split(
                    settings.DB_BACKUP_CMD,  # nosec
                    posix=(name == "posix")))
        from os.path import isdir
        if isdir(settings.DB_BACKUP_LOC):
            from time import gmtime
            from os.path import join, abspath, basename
            from os import listdir, remove
            now = gmtime()
            dbname = basename(settings.DATABASES['default']['NAME'])
            bkname = "{y}-{m}-{d}_".format(
                y=now.tm_year, m=now.tm_mon, d=now.tm_mday) + dbname
            with open(settings.DATABASES['default']['NAME'], "rb") as d:
                with open(join(settings.DB_BACKUP_LOC, bkname), "wb") as b:
                    b.write(d.read())
            bks = sorted(
                tuple(f for f in listdir(settings.DB_BACKUP_LOC)
                      if dbname in f))
            deletes = len(bks) - settings.DB_BACKUP_MAX_COUNT
            if deletes > 0:
                for f in bks[:deletes]:
                    remove(join(settings.DB_BACKUP_LOC, f))

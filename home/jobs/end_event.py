"""
End the exchange.
"""

from .phase_job_template import PhaseJob


class Job(PhaseJob):
    help = "Delete proof, unset all user information."

    def execute(self):
        from django.conf import settings
        from users.models import User, Proof
        from subs.models import subreddit
        from historicals.models import Memorial, seasons
        from allauth.socialaccount.models import SocialToken
        from os.path import join
        from os import listdir, remove, rmdir
        from admin_extras.models import Task
        from time import gmtime

        # Finish cleaning up from ship phase 2
        year = gmtime().tm_year
        for user in User.objects.exclude(kouhai=None).all():
            if user.verified_proof:
                # Add this santa to the whitelist
                user.whitelist_until_year = max(year,
                                                user.whitelist_until_year) + 2
            else:
                # Add this santa to the blacklist
                user.banned_until_year = max(year, user.banned_until_year) + 2
            # Unset this santa's kouhai
            user.kouhai = None
            user.save()

        # Reset each user to nearly what they were when they signed up.
        User.objects.update(
            location=None,
            kouhai=None,
            regifter=False,
            international=False,
            subreddit=None,
            completed_settings=False,
            address_info="",
            done_submitting_proof=False,
            verified_proof=False,
            age=None,
            karma=None,
            checked_karma=False,
            #banned_until_year ----\
            #whitelist_until_year ----- Not reset for obvious reasons
            accepted_into_exchange=False,
            total_uploaded=0,
            sponsorship_invites=0,
            is_sponsored=False,
            awaiting_token_refresh=False,
        )
        # Deactivate the non-admin user accounts until next year
        #                                          id 1 is anonuser
        User.objects.filter(is_staff=False).filter(id__gt=1).update(
            is_active=False)
        # Delete all access tokens for all users
        SocialToken.objects.all().delete()
        # Delete all proof submitted by users
        # First, database entries
        Proof.objects.all().delete()
        # Then, files and folders
        for d in listdir(settings.MEDIA_ROOT):
            for f in listdir(join(settings.MEDIA_ROOT, d)):
                remove(join(settings.MEDIA_ROOT, d, f))
            rmdir(join(settings.MEDIA_ROOT, d))
        # Remove all admin tasks
        Task.objects.all().delete()
        # Set all subreddits to not participating
        subreddit.objects.update(participating=False)
        # Make the yearbook visible
        now = gmtime()
        Memorial.objects.filter(
            year=now.tm_year, season=seasons[now.tm_mon]).update(visible=True)

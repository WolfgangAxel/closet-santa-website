"""
End the first shipping phase
"""

from .phase_job_template import PhaseJob


class Job(PhaseJob):
    help = "Move site out of first shipping phase"

    def execute(self):
        from home import PhaseSettings

        PhaseSettings.switch_to("No signups")

        self.panic(
            "The first shipping phase has ended",
            "In 12 hours, the website will officially move people " +
            "out of the exchange. Please be sure to have all proof verified by then."
        )

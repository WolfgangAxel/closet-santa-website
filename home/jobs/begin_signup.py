"""
Reactivates all users, allow for signups
"""

from .phase_job_template import PhaseJob


class Job(PhaseJob):
    help = "Move site to the signup phase, unfreeze accounts"

    def execute(self):
        from users.models import User
        from home import PhaseSettings
        from historicals.models import Memorial, seasons
        from time import gmtime

        PhaseSettings.switch_to("Signups")
        # Welcome back everyone!
        User.objects.filter(is_active__exact=False).update(is_active=True)
        # Make a yearbook for this event
        now = gmtime()
        Memorial.objects.create(year=now.tm_year, season=seasons[now.tm_mon])

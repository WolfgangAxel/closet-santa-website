"""
Assign kouhais, move to second shipping phase
"""
from .phase_job_template import PhaseJob


class Job(PhaseJob):
    help = "Move site to second shipping phase"

    def execute(self):
        from users.models import User
        from admin_extras.models import Task
        from time import gmtime
        from home.jobs.cost_calculator import assignment
        from home import PhaseSettings

        PhaseSettings.switch_to("Second shipment")
        for task in Task.objects.filter(
                title__exact=
                "It's time to panic! There are not enough regifters!").all():
            task.complete = True
            task.save()
        # Anyone still left in the exchange needs a new santa
        needs_regifting = tuple(
            User.objects.filter(accepted_into_exchange=True).all())
        # Get regifters who weren't just banned
        # Also don't get regifters who didn't get a gift themselves, because that's just unfair
        year = gmtime().tm_year
        regifters = User.objects.filter(regifter=True,
                                        accepted_into_exchange=False,
                                        verified_proof=True,
                                        banned_until_year__lte=year)
        # Get the new list of users as a text string,
        # save it, then notify the messaging bot
        listdata = assignment(needs_regifting, regifters)
        self.notify_bot(listdata)
        # All kouhai were reset when the regift status job was ran,
        # so regifters are the only people with kouhai at this point.
        santa_pool = User.objects.exclude(kouhai=None).all()
        self.notify_users(santa_pool,
                          "You have been selected to be a regifter!",
                          "markdown/regifter_notification.md")
        # We can also use the regifter to get the list of kouhai still
        # in need of a gift
        self.notify_users([u.kouhai for u in santa_pool],
                          "You have been assigned a new Santa!",
                          "markdown/second_santa_notification.md")

"""
In an emergency, export the santa list
"""

from django_extensions.management.jobs import BaseJob


class Job(BaseJob):
    help = "In an emergency, export the santa list. Save this as `santaList.csv` on the messaging bot server"

    def execute(self):
        from users.models import User
        print("Kouhai,Santa")
        for user in User.objects.exclude(kouhai=None).all():
            print("{0},{1}".format(user.kouhai.username, user.username))

"""
Move site to post-signup phase
"""

from .phase_job_template import PhaseJob


class Job(PhaseJob):
    help = "Move site to post-signup phase"

    def execute(self):
        from home import PhaseSettings
        PhaseSettings.switch_to("No signups")

"""
Go through new users and detract karma from the karma total from karmawhore subs

Due to its tight integration with praw and the difficulties that come with testing praw, 
this module is not currently tested
"""

from home.jobs.phase_job_template import PhaseJob


class Job(PhaseJob):
    help = "Adjust karma for karmawhoring"

    _reddit = None

    def get_reddit_user(self, **kwargs):
        """
        Gets a reddit user through praw
        
        Intended to be overwritten by tests
        """
        if self._reddit == None:
            from praw import Reddit
            self._reddit = Reddit
        return self._reddit(**kwargs).user.me()

    def execute(self):
        from users.models import User
        from users.views import get_context
        from subs.models import subreddit
        from allauth.socialaccount.models import SocialApp
        from django.conf import settings
        from time import gmtime
        from datetime import datetime

        app = SocialApp.objects.filter(provider__iexact="reddit").first()
        if not app:
            # We should never be here if things are configured properly
            # Raise an error in hopes that the output is seen anyway
            raise Exception(
                "No provider for Reddit, the site is probably misconfigured. "
                + "Add Reddit as a SocialApplication in the Admin settings.")
        cid = app.client_id
        sec = app.secret

        allowed_subs = settings.ALLOWED_SUBS + [
            sub.title.lower() for sub in subreddit.objects.all()
        ]

        for user in User.objects.filter(
                # only non-banned users
                banned_until_year__lte=gmtime().tm_year,
                # only users we haven't checked
                checked_karma=False,
                # only users who have completed their settings
                completed_settings=True).all():
            if user.socialaccount_set.count() == 0:
                # user has no socialaccount, so there is no reddit account associated with it
                continue
            socialaccount = user.socialaccount_set.last()
            if socialaccount.socialtoken_set.count() == 0:
                # socialaccount has no socialtoken, which AFAIK shouldn't happen but who knows...
                continue
            # Get the refresh token that was created when they logged in
            # Use that to create a praw instance to access account information
            socialtoken = socialaccount.socialtoken_set.last()
            if socialtoken.expires_at.timestamp() < datetime.utcnow(
            ).timestamp():
                if not user.awaiting_token_refresh:
                    user.awaiting_token_refresh = True
                    user.save()
                    self.notify_user(
                        user,
                        "Please sign back in to the Closet Santa website",
                        "markdown/refresh_needed.md")
                continue
            Ruser = self.get_reddit_user(
                client_id=cid,
                client_secret=sec,
                refresh_token=socialtoken.token_secret,
                user_agent=settings.SOCIALACCOUNT_PROVIDERS['reddit']
                ['USER_AGENT'].replace("django", "praw"))
            totalKarma = socialaccount.extra_data[
                'link_karma'] + socialaccount.extra_data['comment_karma']
            deductions = 0
            # Remove karma from a user's top 25 submissions if they were from ignored subs
            for sub in Ruser.submissions.top(limit=25):
                if sub.subreddit.display_name.lower(
                ) in settings.IGNORED_SUBREDDITS:
                    deductions += sub.score
            # Remove karma from a user's top 50 comments if they were from ignored subs
            for com in Ruser.comments.top(limit=50):
                if com.subreddit.display_name.lower(
                ) in settings.IGNORED_SUBREDDITS:
                    deductions += com.score
            # Add 1 karma for every thing they've upvoted in acceptable subs
            for upvote in Ruser.upvoted(limit=100):
                if upvote.subreddit.display_name.lower() in allowed_subs:
                    totalKarma += 1
            user.karma = totalKarma - deductions
            c = get_context(user)
            user.accepted_into_exchange = (c['pass'] == 'Yes')
            user.checked_karma = True
            user.awaiting_token_refresh = False
            user.sponsorship_invites = 3 * int(c['pass'] == 'Yes')
            user.save()
            if user.accepted_into_exchange:
                self.notify_user(
                    user,
                    "You have been accepted into the Closet Santa Exchange!",
                    "markdown/acceptance_letter.md")
            else:
                self.notify_user(
                    user,
                    "You have not been accepted into the Closet Santa Exchange",
                    "markdown/rejection_letter.md",
                    extra_context=c)

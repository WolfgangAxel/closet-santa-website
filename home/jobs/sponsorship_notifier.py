"""
Notify users when they are sponsored.

This should be ran as a cronjob at a frequent rate (5-15 minutes)
"""

from .phase_job_template import PhaseJob


class Job(PhaseJob):
    help = "Notify users of sponsorship"

    def execute(self):
        from sponsorship.models import Invitation

        invites = Invitation.objects.filter(notified=False)
        if invites.exists():
            for invite in invites.all():
                self.notify_user(invite.invitee,
                                 "You have been sponsored into the exchange!",
                                 "markdown/sponsorship.md",
                                 extra_context={"sponsor": invite.sponsor})
            invites.update(notified=True)

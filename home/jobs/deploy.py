"""
Overwrite `settings.py` with different values
"""

from .phase_job_template import PhaseJob

mod_perms = {
    "admin_extras | task": ["Can change task", "Can view task"],
    "historicals | memorial":
    ["Can add memorial", "Can change memorial", "Can view memorial"],
    "historicals | quote":
    ["Can add quote", "Can change quote", "Can view quote"],
    "subs | subreddit":
    ["Can add subreddit", "Can change subreddit", "Can view subreddit"],
    "subs | series":
    ["Can add series", "Can change series", "Can view series"],
    "users | user": ["Can change user", "Can view user"],
    "sponsorship | invitation": ["Can view invitation"],
}


class Job(PhaseJob):
    help = "Ready site for production deployment"

    def execute(self):
        from django.conf import settings
        from home import PhaseSettings
        from os.path import join
        from django.contrib.auth.models import Group, Permission
        # Create the exchange phases
        PhaseSettings.initialize()
        # Create the auth groups
        kami = Group.objects.create(name="Kami-sama")
        mods = Group.objects.create(name="Mod-sama")
        for p in Permission.objects.all():
            kami.permissions.add(p)
            if p.name in mod_perms.get(p.content_type, []):
                mods.permissions.add(p)

        # Initialize the settings file
        self.update_settings_file(
            join(settings.BASE_DIR, 'ClosetSanta', 'settings.py'))

    def update_settings_file(self, fp):
        """
        In its own function for easy mocking during tests
        """
        from django.conf import settings
        from os.path import join
        from django.contrib.auth.base_user import BaseUserManager
        from pathlib import Path
        SetFile = ""
        with open(fp, 'r') as f:
            SetFile = f.read()
        with open(fp, 'w') as f:
            # Turn off debugging
            SetFile = SetFile.replace("\nDEBUG = True", "\nDEBUG = False")
            # Generate a real secret key
            b = BaseUserManager()
            SetFile = SetFile.replace(
                "\nSECRET_KEY = 'CHANGE_ME_BEFORE_YOU_LAUNCH'",
                "\nSECRET_KEY = '{}'".format(
                    b.make_random_password(length=48)))
            f.write(SetFile)
        # Restart the server
        Path(join(settings.BASE_DIR, 'ClosetSanta', 'wsgi.py')).touch()

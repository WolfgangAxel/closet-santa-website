from praw import Reddit

# Fill this out with the pertenent information
R = Reddit(  # nosec
    username="",
    password="",
    client_id="",
    client_secret="",
    user_agent=
    "Closet Santa Website to Message Bot helper V0.9 | by {{your personal username}}"
)

# This is the username of your RSSB messaging bot (no /u/ prefix)
m_bot = ""

from django_extensions.management.jobs import BaseJob


class Job(BaseJob):
    help = "Dummy job, this is a helper function"

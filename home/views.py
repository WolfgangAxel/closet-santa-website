from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.conf import settings
from subs.models import subreddit
from admin_extras.views import task_notifs
from . import PhaseSettings

# Create your views here.


@task_notifs
def landing(req):
    return render(req, PhaseSettings.get_home_template())


@task_notifs
def info(req):
    return render(req, 'home/infopage.html')


@task_notifs
def security(req):
    return render(req, 'home/secupage.html')


@task_notifs
def contact(req):
    return render(req, 'home/contact.html')


@task_notifs
def karma_adj(req):
    context = {
        'ignored': [sub.title() for sub in settings.IGNORED_SUBREDDITS],
        'allowed':
        sorted([sub.title() for sub in settings.ALLOWED_SUBS] +
               [sub.title for sub in subreddit.objects.all()])
    }
    return render(req, 'home/karma_adjust.html', context)


def bounce(req):
    if req.user.is_authenticated:
        return redirect('u/' + req.user.username)
    else:
        return redirect('/')


theme_switcher = {
    "/static/home/cs.css": "/static/home/cs-lt.css",
    "/static/home/cs-lt.css": "/static/home/cs.css",
    "/static/home/bg.gif": "/static/home/bg-lt.gif",
    "/static/home/bg-lt.gif": "/static/home/bg.gif",
}


def theme_toggle(req):
    css = req.session.get("css", "/static/home/cs.css")
    bg = req.session.get("bg", "/static/home/bg.gif")
    req.session["css"] = theme_switcher[css]
    req.session["bg"] = theme_switcher[bg]
    return HttpResponse(status=200)

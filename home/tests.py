# Testing all the jobs here would get messy
from .job_tests.check_regift_status import CRGSTest
from .job_tests.deploy import DeployTest
from .job_tests.cost_calculator import MatchingTest
from .job_tests.begin_signup import BeginSignupTest
from .job_tests.begin_ship_1 import BeginShip1Test
from .job_tests.end_ship_1 import EndShip1Test
from .job_tests.begin_ship_2 import BeginShip2Test
from .job_tests.end_ship_2 import EndShip2Test
from .job_tests.end_event import EndEventTest
from .job_tests.Karma_Validator import KarmaValidatorTest
from .job_tests.sponsorship_notifier import SponsorshipNotifierTest
from .job_tests.praw import PrawTest

from django.test import TestCase, Client
from . import PhaseSettings as PS
from .models import PhaseSettings
from users.models import User


class PhaseSettingsTest(TestCase):
    """
    The PhaseSettings module provides handy shortcuts to interacting with
    the current site Phase.
    """

    def test_1_initialization_works(self):
        PS.initialize()
        self.assertEqual(PhaseSettings.objects.count(), 4)
        self.assertEqual(PhaseSettings.objects.filter(active=True).count(), 1)
        active = PhaseSettings.objects.get(active=True)
        self.assertEqual(active.title, "No signups")

    def test_2_switching_works(self):
        # Should return the phase requested
        PS.switch_to("First shipment")
        self.assertEqual(PhaseSettings.objects.filter(active=True).count(), 1)
        active = PhaseSettings.objects.get(active=True)
        self.assertEqual(active.title, "First shipment")
        # As a failsafe, typos send the site back to no signups
        # to leave as few avenues enabled as possible while the issue is fixed.
        PS.switch_to("Not a real phase")
        self.assertEqual(PhaseSettings.objects.filter(active=True).count(), 1)
        active = PhaseSettings.objects.get(active=True)
        self.assertEqual(active.title, "No signups")

    def test_3_all_shortcuts_return_expected_data(self):
        PS.switch_to("Signups")
        self.assertEqual(PS.get_title(), "Signups")
        self.assertEqual(PS.get_home_template(), "home/homepage_signup.html")
        self.assertTrue(PS.get_signups_allowed())
        self.assertFalse(PS.get_proof_can_be_submitted())

    def test_4_site_reflects_phase(self):
        C = Client()
        PS.switch_to("No signups")
        response = C.get("/", follow=True)
        self.assertIn("home/homepage_no_signup.html",
                      [t.name for t in response.templates])
        PS.switch_to("First shipment")
        response = C.get("/", follow=True)
        self.assertIn("home/homepage_ship_1.html",
                      [t.name for t in response.templates])


class ViewsTest(TestCase):

    def setUp(self):
        self.c = Client()

    def test_1_themes(self):
        res = self.c.get("/")
        self.assertIn(b"/static/home/cs.css", res.content)
        self.c.get("/theme_toggle")
        res = self.c.get("/")
        self.assertIn(b"/static/home/cs-lt.css", res.content)

    def test_2_correct_templates(self):

        def t_names(templates):
            return [t.name for t in templates]

        def test_template(endpoint, template):
            res = self.c.get(endpoint)
            self.assertIn(template, t_names(res.templates))

        PS.initialize()
        test_template("/", PS.get_home_template())
        test_template("/information", "home/infopage.html")
        test_template("/security", "home/secupage.html")
        test_template("/contact", "home/contact.html")
        test_template("/karma_adjust", "home/karma_adjust.html")

    def test_3_bounce(self):
        res = self.c.get("/bounce", follow=True)
        self.assertIn("/", res.redirect_chain[-1])
        u = User.objects.create(username="user")
        self.c.force_login(u)
        res = self.c.get("/bounce", follow=True)
        self.assertIn("/u/user/", res.redirect_chain[-1])
        self.c.logout()

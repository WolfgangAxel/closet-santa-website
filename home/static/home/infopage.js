// Holder for dates
var cal = {}
var done_count = 0

// Do stuff when the document is loaded...
function stuff (ID) {
    done_count++;
    var tag = document.getElementById(ID);
    tag.title = tag.innerHTML;
    cal[ID] = {
        date: new Date(tag.innerHTML),
        id: ID.slice(0,-5),
        expired: false
    }
    tag.innerHTML = cal[ID].date.toLocaleDateString(undefined, { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric', hour: '2-digit', minute: '2-digit', second: '2-digit', timeZoneName:'short' });
}

// ... to these elements
["signup-start-date", "signup-end-date", "pairing-happens-date", "pop-due-date", "rematch-start-date", "rematch-pop-due-date"].forEach(stuff)

// Calculating time remaining
function maths() {
    for (v in cal) {
        if (cal[v].expired == false) {
          // Get todays date and time
          var now = new Date().getTime();

          // Find the distance between now and the count down date
          var distance = cal[v].date - now;

          // Time calculations for days, hours, minutes and seconds
          var days = Math.floor(distance / (1000 * 60 * 60 * 24));
          var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
          var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
          var seconds = Math.floor((distance % (1000 * 60)) / 1000);

          // If the count down is finished, write some text
          if (distance > 0) {
                document.getElementById(cal[v].id).innerHTML = days + "d " + hours + "h "
                    + minutes + "m " + seconds + "s ";
          } else {
                document.getElementById(cal[v].id).innerHTML = "EXPIRED";
                cal[v].expired = true;
                done_count--;
          };
        };
    if (done_count == 0) {
        clearInterval(x);
    };
  };
};
// Update the count down every 1 second
var x = setInterval(maths, 1000);
// Do it when the page loads
maths();

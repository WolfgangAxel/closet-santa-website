function toggle_theme() {
    var theme = document.getElementById("theme");
    var style = document.getElementById("htmlstyle");
    if (theme.href.includes("lt")) {
        theme.href = "/static/home/cs.css";
        style.innerHTML = "html {background-color: var(--bg); color: var(--text); background-image: url(/static/home/bg.gif)}";
    } else {
        theme.href = "/static/home/cs-lt.css";
        style.innerHTML = "html {background-color: var(--bg); color: var(--text); background-image: url(/static/home/bg-lt.gif)}";
    }
    var req = new XMLHttpRequest();
    req.open("GET", "/theme_toggle");
    req.setRequestHeader("Content-Type", "text/plain");
    req.send("");
}

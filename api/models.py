from django.db import models
from uuid import uuid4


class Nonce(models.Model):
    uuid = models.UUIDField(primary_key=True, default=uuid4, editable=False)
    listdata = models.TextField()

from django.shortcuts import render
from .models import Nonce
from django.http import HttpResponse


def api_getlist(req, uuid):
    """
    A view for the messaging bot to request the data for the new pairings list.
    """
    try:
        n = Nonce.objects.get(uuid=uuid)
    except:
        return HttpResponse(status=404)
    n.delete()
    return HttpResponse(str(n.listdata), content_type="text/plain")

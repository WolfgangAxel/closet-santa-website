from django.urls import path
from . import views

urlpatterns = [
    path("list/<uuid>", views.api_getlist),
]

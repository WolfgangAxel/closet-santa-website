from django.test import TestCase, Client
from .models import Nonce


class NonceViewTests(TestCase):

    def setUp(self):
        self.c = Client()

    def test_1_nonce_flow_works(self):
        n = Nonce.objects.create(
            listdata="VHJhbnMgcmlnaHRzIGFyZSBodW1hbiByaWdodHMh")
        res = self.c.get(f"/api/list/{n.uuid}", follow=True)
        self.assertEqual(res.content,
                         b"VHJhbnMgcmlnaHRzIGFyZSBodW1hbiByaWdodHMh")
        self.assertEqual(Nonce.objects.count(), 0)

from django.test import TestCase, Client
from django.urls import reverse
from django.forms import ValidationError
from django.contrib.sites.models import Site
from allauth.socialaccount.models import SocialAccount
from .models import User, image_size, Proof
from .views import get_context
from subs.models import subreddit, series
from admin_extras.models import Task
from time import time, gmtime
from django.conf import settings
from os.path import join


class UserTests(TestCase):

    def test_accept_user(self):
        """
        Test various situations where a user will and will not be accepted
        """

        def process(yesno, **kwargs):
            u = User.objects.create(**kwargs)
            c = get_context(u)
            self.assertEqual(c['pass'], yesno)

        process("Yes",
                username="test_generic_success",
                karma=30000,
                age=time() - 30 * 30 * 24 * 60 * 60)
        process("Yes",
                username="test_whitelist_success",
                karma=0,
                age=time(),
                whitelist_until_year=gmtime().tm_year + 5)
        process("No", username="test_double_failure", karma=0, age=time())
        process("No", username="test_age_failure", karma=30000, age=time())
        process("No",
                username="test_karma_failure",
                karma=0,
                age=time() - 30 * 30 * 24 * 60 * 60)
        process("No",
                username="test_banned_failure",
                karma=30000,
                age=time() - 30 * 30 * 24 * 60 * 60,
                banned_until_year=gmtime().tm_year + 5)


class ImageSizeTests(TestCase):

    def setUp(self):

        class fake_file(object):

            def __init__(fileself, content_type):
                fileself.content_type = content_type

        class fake_image(object):

            def __init__(imgself, file, size):
                imgself.file = file
                imgself.size = size

        self.file = fake_file
        self.image = fake_image

    def test_1_allowed_filesize(self):
        """
        Test to make sure that images can be up to but not exceeding the defined single file size
        """
        im1 = self.image(self.file("image/jpeg"),
                         int(settings.MAX_UPLOAD_SIZE / 2))
        im2 = self.image(self.file("image/jpeg"), settings.MAX_UPLOAD_SIZE)
        im3 = self.image(self.file("image/jpeg"), settings.MAX_UPLOAD_SIZE * 2)
        self.assertIsNotNone(image_size(im1))
        self.assertIsNotNone(image_size(im2))
        self.assertRaises(ValidationError, image_size, im3)

    def test_2_allowed_filetypes(self):
        """
        Test to make sure only desired filetypes are allowed, and that specific formats aren't checked.
        """
        im1 = self.image(self.file("image/jpeg"), settings.MAX_UPLOAD_SIZE)
        im2 = self.image(self.file("image/png"), settings.MAX_UPLOAD_SIZE)
        im3 = self.image(self.file("text/plain"), settings.MAX_UPLOAD_SIZE)
        self.assertIsNotNone(image_size(im1))
        self.assertIsNotNone(image_size(im2))
        self.assertRaises(ValidationError, image_size, im3)


class ViewsTests(TestCase):

    def setUp(self):
        self.c = Client()
        self.current_site = Site.objects.get_current()
        self.socialapp = self.current_site.socialapp_set.create(  # nosec
            provider="reddit",
            name="reddit",
            client_id="1234567890",
            secret="0987654321",
        )
        self.ser = series.objects.create(name="ser")
        self.sub = subreddit.objects.create(title="subsub",
                                            related_series=self.ser)
        self.image = open(
            join(settings.BASE_DIR, "home", "static", "home", "bg.gif"), "rb")
        self.omnibli = User.objects.create(is_staff=True,
                                           username="omnipotence_is_bliss")
        SocialAccount.objects.create(user=self.omnibli,
                                     provider="reddit",
                                     uid="omnipotence_is_bliss",
                                     extra_data={"created_utc": time()})
        self.user_one = User.objects.create(kouhai=self.omnibli,
                                            username="user1")
        SocialAccount.objects.create(user=self.user_one,
                                     provider="reddit",
                                     uid="user1",
                                     extra_data={"created_utc": time()})
        self.user_two = User.objects.create(kouhai=self.user_one,
                                            username="user2")
        SocialAccount.objects.create(user=self.user_two,
                                     provider="reddit",
                                     uid="user2",
                                     extra_data={"created_utc": time()})

    def tearDown(self):
        self.image.close()

    def test_01_anonymous_profile_failure(self):
        r = self.c.get("/u/user1", follow=True)
        self.assertIn("users/profile_error.html",
                      tuple(t.name for t in r.templates))

    def test_02_admin_can_see_all(self):
        self.c.force_login(self.omnibli)
        r1 = self.c.get("/u/user1", follow=True)
        self.assertIn("users/kouhai.html", tuple(t.name for t in r1.templates))
        r2 = self.c.get("/u/user2", follow=True)
        self.assertIn("users/kouhai.html", tuple(t.name for t in r2.templates))
        self.c.logout()

    def test_03_user_can_see_only_kouhai(self):
        self.c.force_login(self.user_one)
        r1 = self.c.get("/u/omnipotence_is_bliss", follow=True)
        self.assertIn("users/kouhai.html", tuple(t.name for t in r1.templates))
        r2 = self.c.get("/u/user2", follow=True)
        self.assertNotIn("users/kouhai.html",
                         tuple(t.name for t in r2.templates))
        self.c.logout()

    def test_04_user_can_access_profile(self):
        self.c.force_login(self.user_two)
        r = self.c.get("/u/user2", follow=True)
        self.assertIn("users/profile_template.html",
                      tuple(t.name for t in r.templates))
        self.c.logout()

    def test_05_user_OTS(self):
        self.c.force_login(self.user_two)
        r1 = self.c.get("/u/user2", follow=True)
        self.assertIn("users/OTS.html", tuple(t.name for t in r1.templates))
        self.user_two.completed_settings = True
        self.user_two.save()
        r2 = self.c.get("/u/user2", follow=True)
        self.assertNotIn("users/OTS.html", tuple(t.name for t in r2.templates))
        self.c.logout()

    def test_06_OTS_flow_works(self):
        self.c.force_login(self.user_two)
        formdata = {
            "location": "US",
            "regifter": True,
            "international": True,
            "subreddit": 1,
            "confirm": True
        }
        res = self.c.post("/u/user2/form-response-settings/",
                          formdata,
                          follow=True)
        self.assertEqual(res.redirect_chain[-1][0], "/u/user2/")
        self.assertIn(b"Your account has been updated", res.content)
        self.assertNotIn("users/OTS.html",
                         tuple(t.name for t in res.templates))
        self.user_two = User.objects.get(username="user2")
        self.assertEqual(self.user_two.location, "US")
        self.assertEqual(self.user_two.subreddit, self.sub)
        self.assertTrue(self.user_two.regifter)
        self.assertTrue(self.user_two.international)

    def test_07_OTS_failure_flow_works(self):
        self.c.force_login(self.user_two)
        formdata = {
            "regifter": True,
            "international": True,
            "subreddit": 1,
            "confirm": True
        }
        res = self.c.post("/u/user2/form-response-settings/",
                          formdata,
                          follow=True)
        self.assertEqual(res.redirect_chain[-1][0], "/u/user2/")
        self.assertIn(
            b"Your account was not updated because the form was invalid",
            res.content)
        self.user_two = User.objects.get(username="user2")
        self.assertIsNone(self.user_two.location)
        self.assertIsNone(self.user_two.subreddit)
        self.assertFalse(self.user_two.regifter)
        self.assertFalse(self.user_two.international)

    def test_08_address_flow_works(self):
        self.c.force_login(self.user_two)
        formdata = {
            "address_info": "123 east north st",
        }
        res = self.c.post("/u/user2/form-response-address/",
                          formdata,
                          follow=True)
        self.assertEqual(res.redirect_chain[-1][0], "/u/user2/")
        self.assertIn(b"Your shipping information has been updated",
                      res.content)
        self.user_two = User.objects.get(username="user2")
        self.assertEqual(self.user_two.address_info, "123 east north st")

    def test_09_address_failure_flow_works(self):
        self.user_two.completed_settings = True
        self.user_two.save()
        self.c.force_login(self.user_two)
        formdata = {
            "wrong_thing": "idk man",
        }
        res = self.c.post("/u/user2/form-response-address/",
                          formdata,
                          follow=True)
        self.assertEqual(res.redirect_chain[-1][0], "/u/user2/")
        self.assertIn(
            b"Your account was not updated because the form was invalid",
            res.content)
        self.user_two = User.objects.get(username="user2")
        self.assertEqual(self.user_two.address_info, "")

    def test_10_kouhai_proof_upload(self):
        self.c.force_login(self.user_two)
        res = self.c.post("/u/user1/form-response-proof/", {
            "image": self.image,
            "done_submitting_proof": False
        },
                          follow=True)
        self.assertEqual(Proof.objects.count(), 1)
        self.user_two = User.objects.get(username="user2")
        self.assertGreater(self.user_two.total_uploaded, 0)

    def test_11_only_allowed_users_can_see_proof(self):
        self.c.force_login(self.user_two)
        self.c.post("/u/user1/form-response-proof/", {
            "image": self.image,
            "done_submitting_proof": False
        },
                    follow=True)
        res = self.c.get("/u/user2/proof/1")
        self.assertEqual(res.status_code, 200)
        self.c.logout()
        self.c.force_login(self.omnibli)
        res = self.c.get("/u/user2/proof/1")
        self.assertEqual(res.status_code, 200)
        self.c.logout()
        self.c.force_login(self.user_one)
        res = self.c.get("/u/user2/proof/1")
        self.assertEqual(res.status_code, 401)

    def test_12_invalid_images_yield_404(self):
        self.c.force_login(self.user_two)
        res = self.c.get("/u/user2/proof/1")
        self.assertEqual(res.status_code, 404)

    def test_13_upload_fails_when_over_total(self):
        self.user_two.total_uploaded = settings.TOTAL_UPLOADS_PER_USER
        self.user_two.save()
        self.c.force_login(self.user_two)
        res = self.c.post("/u/user1/form-response-proof/", {
            "image": self.image,
            "done_submitting_proof": False
        },
                          follow=True)
        self.assertIn(b"Proof not saved. Not enough space remaining",
                      res.content)

    def test_14_finished_proof_makes_task(self):
        self.c.force_login(self.user_two)
        self.c.post("/u/user1/form-response-proof/", {
            "image": self.image,
            "done_submitting_proof": True
        })
        self.assertTrue(Task.objects.count(), 1)

    def test_15_OTS_only_uses_participating_subs(self):
        self.c.force_login(self.user_two)
        self.sub.participating = False
        self.sub.save()
        r1 = self.c.get("/u/user2", follow=True)
        self.assertNotIn(b"/r/subsub", r1.content)
        self.sub.participating = True
        self.sub.save()
        r2 = self.c.get("/u/user2", follow=True)
        self.assertIn(b"/r/subsub", r2.content)

    def test_16_OTS_fails_for_non_participating_subs(self):
        self.c.force_login(self.user_two)
        self.sub.participating = False
        self.sub.save()
        formdata = {
            "regifter": True,
            "international": True,
            "subreddit": 1,
            "confirm": True
        }
        res = self.c.post("/u/user2/form-response-settings/",
                          formdata,
                          follow=True)
        self.assertIn(
            b"Your account was not updated because the form was invalid",
            res.content)

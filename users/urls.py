from django.urls import path
from . import views

urlpatterns = [
    path("<slug:username>/", views.profile_handler),
    path("<slug:username>/form-response-<slug:formname>/", views.form_handler),
    path("<slug:username>/proof/<int:pid>", views.view_raw_proof),
]

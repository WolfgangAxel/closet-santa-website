from django import forms
from .models import User, Proof
from .country_codes import country_code_tuple
from subs.models import subreddit


class SettingsForm(forms.ModelForm):

    location = forms.ChoiceField(choices=country_code_tuple, required=True)
    subreddit = forms.ModelChoiceField(
        queryset=subreddit.objects.filter(participating=True), required=True)
    confirm = forms.BooleanField(label='I verify that the above is correct',
                                 required=True)

    class Meta:
        model = User
        fields = ('location', 'regifter', 'international', 'subreddit')
        labels = {
            'location': 'My country',
            'regifter': 'I would like to sign up to be a regifter',
            'international':
            'I would be okay with shipping a gift internationally',
            'subreddit': 'I would like to sign up to represent',
        }


class AddressForm(forms.ModelForm):

    class Meta:
        model = User
        fields = ('address_info', )
        labels = {'address_info': 'Shipping information'}


class ProofForm(forms.ModelForm):
    done_submitting_proof = forms.BooleanField(
        label='I am done submitting proof', required=False)

    class Meta:
        model = Proof
        fields = ('image', )
        labels = {'image': 'Choose an image:'}

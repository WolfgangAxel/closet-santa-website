# Didn't want to muddy up `views.py`


def allowed_to_exchange(context):
    if context['banned']:
        return 'No'
    if context['whitelisted']:
        return 'Yes'
    if context['age'] >= 18 and context['totalKarma'] >= 2000:
        return 'Yes'
    return 'No'

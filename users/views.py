from django.shortcuts import render, redirect
from django.contrib import messages
from django.conf import settings
from django.http import HttpResponse

from admin_extras.models import Task
from admin_extras.views import task_notifs
from home import PhaseSettings
from . import allowance_tests, country_codes
from .models import User, Proof
from .forms import SettingsForm, AddressForm, ProofForm
from os.path import split
from time import gmtime, time


def default(func):

    @task_notifs
    def wrapper(req, username, *args, **kwargs):
        # Anonymous users can't see profiles
        if req.user.is_authenticated == False:
            return render(
                req, 'users/profile_error.html',
                {'reason': 'You are not authorized to view this profile.'})
        # Now we search for the requested user
        search = User.objects.filter(username__iexact=username)
        if search.exists() == False:
            return render(req, 'users/profile_error.html',
                          {'reason': 'This user does not exist.'})
        # pass the request, the user found, and any other args to func
        return func(req, search.first(), *args, **kwargs)

    return wrapper


####################################################################


@default
def profile_handler(req, requested_user, *args, **kwargs):
    if req.method != "GET":
        messages.add_message(req, messages.ERROR,
                             "Endpoint is only for GET requests.")
        return redirect(f"/u/{requested_user.username}")
    # Open the personal profile
    if req.user == requested_user:
        return user_profile(req)
    # Open the kouhai profile
    if req.user.kouhai == requested_user or req.user.is_staff:
        return kouhai_profile(req, requested_user)
    # This user does not have permission
    return render(req, 'users/profile_error.html',
                  {'reason': 'You are not authorized to view this profile.'})


@default
def form_handler(req, requested_user, formname, *args, **kwargs):
    if req.method != "POST":
        messages.add_message(req, messages.ERROR,
                             "Endpoint is only used for form submissions.")
        return redirect(f"/u/{requested_user.username}")
    if (formname == "proof" and req.user.kouhai == requested_user):
        return proof_submission(req)
    if (formname == "settings" and req.user == requested_user):
        return settings_submission(req)
    if (formname == "address" and req.user == requested_user):
        return address_submission(req)
    messages.add_message(req, messages.ERROR,
                         "Form not accepted: unknown form submitted")
    return redirect(f"/u/{requested_user.username}")


def view_raw_proof(req, username, pid):
    """
    For viewing the raw image data
    """
    r = HttpResponse()
    if not (req.user.is_staff or req.user.username == username):
        r.status_code = 401
        return r
    search = Proof.objects.filter(id=pid)
    if search.exists() == False:
        r.status_code = 404
        return r
    proof = search.first()
    r['content-type'] = "image/" + proof.image.name.split(".")[-1]
    r['content-size'] = proof.image.size
    r['Content-Disposition'] = 'inline; filename={}'.format(
        split(proof.image.name)[1])
    r['X-Sendfile'] = proof.image.path
    return r


####################################################################


def user_profile(req):
    """
    Loads the personal profile for a user
    """
    context = get_context(req.user)
    if req.user.completed_settings:
        context['address_form'] = AddressForm()
        return render(req, 'users/profile.html', context)
    else:
        context['settings_form'] = SettingsForm()
        return render(req, 'users/OTS.html', context)


def kouhai_profile(req, kouhai):
    """
    Loads the profile for a user for others to see
    """
    context = get_context(kouhai)
    context["able"] = PhaseSettings.get_proof_can_be_submitted()
    if req.user.done_submitting_proof == False and context["able"]:
        context["proof_form"] = ProofForm()
        context['total_uploaded'] = round(
            req.user.total_uploaded / 1024 / 102.4) / 10
        context['total_space'] = round(
            settings.TOTAL_UPLOADS_PER_USER / 1024 / 102.4) / 10
        context['file_size'] = round(
            settings.MAX_UPLOAD_SIZE / 1024 / 102.4) / 10
    return render(req, 'users/kouhai.html', context)


def address_submission(req):
    """
    Update the user's address information.
    Note that the form.is_valid method
    is buggy for this form in particular
    so we do a manual verification
    instead.
    """
    form = AddressForm(req.POST)
    address_info = form.data.get('address_info')
    valid = type(address_info) == str
    #if form.is_valid():
    if valid:
        req.user.address_info = address_info
        req.user.save()
        messages.add_message(req, messages.SUCCESS,
                             "Your shipping information has been updated.")
    else:
        messages.add_message(
            req, messages.ERROR,
            "Your account was not updated because the form was invalid.")
    return redirect(f"/u/{req.user.username}")


def proof_submission(req):
    """
    Checks that the uploaded proof is an image and
    is within size limitations, then saves it if
    it is valid.
    """
    form = ProofForm(req.POST, req.FILES)
    if form.is_valid():
        new_user_usage = req.user.total_uploaded + form.cleaned_data[
            'image'].size
        if settings.TOTAL_UPLOADS_PER_USER < new_user_usage:
            messages.add_message(
                req, messages.ERROR,
                "Proof not saved. Not enough space remaining.")
            return redirect(f"/u/{req.user.kouhai.username}")
        req.user.proof_set.create(user=req.user,
                                  image=form.cleaned_data['image'])
        req.user.total_uploaded = new_user_usage
        req.user.done_submitting_proof = form.cleaned_data[
            'done_submitting_proof']
        req.user.save()
        if req.user.done_submitting_proof:
            Task.objects.create(
                title="/u/{} needs proof validated!".format(req.user.username),
                description=("<a href='/admin/users/user/{}/change/'>" +
                             "Click here to see what was uploaded</a>. " +
                             "After checking the proofs, check the " +
                             "'Verified Proof' box and press 'save' " +
                             "at the bottom of the page.").format(req.user.id))
        messages.add_message(req, messages.SUCCESS,
                             "Proof saved successfully!")
    else:
        messages.add_message(req, messages.ERROR,
                             "Proof not saved. Invalid type or too large.")
    return redirect(f"/u/{req.user.kouhai.username}")


def settings_submission(req):
    form = SettingsForm(req.POST, instance=req.user)
    if form.is_valid():
        context = get_context(req.user)
        if context["pass"]:
            req.user.completed_settings = True
        # This also saves the user model
        form.save()
        messages.add_message(req, messages.SUCCESS,
                             "Your account has been updated.")
    else:
        messages.add_message(
            req, messages.ERROR,
            "Your account was not updated because the form was invalid.")
    return redirect(f"/u/{req.user.username}")


####################################################################


def get_context(user):
    context = {
        'username':
        user.username.title(),
        'totalKarma':
        user.karma if user.karma else 0,
        'banned':
        user.banned_until_year > gmtime().tm_year,
        'banned_until':
        user.banned_until_year,
        'whitelisted':
        user.whitelist_until_year > gmtime().tm_year,
        'whitelisted_until':
        user.whitelist_until_year,
        'address_info':
        user.address_info,
        'accepted_into_exchange':
        'Yes' if user.accepted_into_exchange else 'No',
        'representing':
        user.subreddit,
        'international':
        user.international,
        'regifter':
        user.regifter,
        'location':
        country_codes.country_code_dict.get(user.location,
                                            "the Great Unknown"),
        'is_mod':
        user.is_staff
    }
    if not user.age:
        # Get the account age from the extra data from logging in
        u_social = user.socialaccount_set.first()  # Get the SocialAccount
        if u_social:
            user.age = int(u_social.extra_data['created_utc'])
            context['age'] = int(
                (time() - user.age) / 60 / 60 / 24 / (365 / 12))
            user.save()
        else:
            # No socialaccount, this user did not sign up through allauth
            # Default age to be 0 months old
            context['age'] = 0
    else:
        context['age'] = int((time() - user.age) / 60 / 60 / 24 / (365 / 12))
    context['pass'] = allowance_tests.allowed_to_exchange(context)
    return context

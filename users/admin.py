from django import forms
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.utils.html import format_html

from os import listdir, path

from .models import User
from .country_codes import country_code_tuple
from subs.models import subreddit as sub_model


class UserChangeForm(forms.ModelForm):
    """A form for updating users. Includes all the fields on
    the user, but replaces the password field with admin's
    password hash display field.
    """

    location = forms.ChoiceField(choices=country_code_tuple, required=False)
    subreddit = forms.ModelChoiceField(queryset=sub_model.objects,
                                       empty_label="Not set",
                                       required=False)
    kouhai = forms.ModelChoiceField(queryset=User.objects,
                                    empty_label="Not set",
                                    required=False)

    class Meta:
        model = User
        fields = (
            'username',
            'location',
            'kouhai',
            'regifter',
            'international',
            'subreddit',
            'completed_settings',
            'banned_until_year',
            'accepted_into_exchange',
            'karma',
            'whitelist_until_year',
            'is_active',
            'is_staff',
            'groups',
            'checked_karma',
            'done_submitting_proof',
            'sponsorship_invites',
            'verified_proof',
        )


class UserAdmin(BaseUserAdmin):
    # The forms to add and change user instances
    form = UserChangeForm
    readonly_fields = ('proofs', 'username', 'kouhai_link', 'karma', 'age',
                       'sponsored_users')

    def proofs(self, user):
        out = "<ul>"
        for p in user.proof_set.all():
            out += "<li><a href='/u/{0}/proof/{2}'>{3}</a> | <a href='/admin/extras/proof-delete/{1}/{2}'>Delete</a></li>".format(
                user.username, user.id, p.id, p.image.name)
        return format_html(out + "</ul>")

    def kouhai_link(self, user):
        if user.kouhai:
            out = "<a href='/admin/users/user/{0}/change/'>{1}</a>".format(
                user.kouhai.id, user.kouhai.username)
        else:
            out = "<p>No kouhai</p>"
        return format_html(out)

    def sponsored_users(self, user):
        out = "<ul>"
        for invite in user.sponsor_invites.all():
            out += f"<li>{invite.invitee.username}</li>"
        return format_html(out + "</ul>")

    # The fields to be used in displaying the User model.
    # These override the definitions on the base UserAdmin
    # that reference specific fields on auth.User.
    list_display = (
        'username',
        'regifter',
        'international',
        'completed_settings',
        'accepted_into_exchange',
        'done_submitting_proof',
        'verified_proof',
        'banned_until_year',
        'whitelist_until_year',
    )
    list_filter = (
        'regifter',
        'international',
        'accepted_into_exchange',
        'done_submitting_proof',
        'verified_proof',
        'banned_until_year',
        'whitelist_until_year',
        'is_staff',
    )
    fieldsets = (
        (None, {
            'fields': (
                'username',
                'karma',
                'age',
                'checked_karma',
                'kouhai_link',
                'banned_until_year',
                'whitelist_until_year',
            )
        }),
        ('Settings', {
            'fields': (
                'location',
                'regifter',
                'international',
                'subreddit',
                'completed_settings',
                'accepted_into_exchange',
            )
        }),
        ('Proof', {
            'fields': (
                'proofs',
                'done_submitting_proof',
                'verified_proof',
            )
        }),
        ('Permissions', {
            'fields': (
                'is_staff',
                'groups',
            )
        }),
    )
    search_fields = ('username', )
    ordering = ('username', )
    filter_horizontal = ()

    def get_fieldsets(self, request, obj=None):
        if request.user.groups.filter(
                name="Kami-sama").exists() or request.user.is_superuser:
            return self.fieldsets
        return self.fieldsets[:-1]


# Now register the new UserAdmin...
admin.site.register(User, UserAdmin)

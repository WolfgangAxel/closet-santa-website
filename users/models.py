from django.db import models
from django.forms import ValidationError
from django.conf import settings
from django.template.defaultfilters import filesizeformat
from django.utils.translation import gettext_lazy
from django_extensions.db import models as ext_models
from django.contrib.auth.models import AbstractUser
# Create your models here.

from time import gmtime
from .country_codes import country_code_tuple
from subs.models import subreddit
from os.path import join, splitext

# Create your models here.


def user_directory_path(instance, filename):
    # file will be uploaded to MEDIA_ROOT/u_<username>/<filename>
    fn = instance.created.strftime('%Y-%m-%d--%H.%M.%S%z') + splitext(
        filename)[1]
    return join("u_" + instance.user.username, fn)


def get_curent_year():
    return gmtime().tm_year


class User(AbstractUser):
    location = models.CharField(max_length=2,
                                choices=country_code_tuple,
                                null=True)
    kouhai = models.ForeignKey('self', null=True, on_delete=models.SET_NULL)
    regifter = models.BooleanField(default=False)
    international = models.BooleanField(default=False)
    subreddit = models.ForeignKey(subreddit,
                                  null=True,
                                  on_delete=models.SET_NULL)
    completed_settings = models.BooleanField(default=False)
    address_info = models.TextField(blank=True)
    done_submitting_proof = models.BooleanField(default=False)
    verified_proof = models.BooleanField(default=False)
    age = models.IntegerField(null=True)
    karma = models.IntegerField(null=True)
    checked_karma = models.BooleanField(default=False)
    banned_until_year = models.IntegerField(default=get_curent_year)
    whitelist_until_year = models.IntegerField(default=get_curent_year)
    accepted_into_exchange = models.BooleanField(default=False)
    total_uploaded = models.IntegerField(default=0)
    sponsorship_invites = models.IntegerField(default=0)
    is_sponsored = models.BooleanField(default=False)
    awaiting_token_refresh = models.BooleanField(default=False)


def image_size(image):
    # Store content type for serving later
    image.content_type = image.file.content_type
    image_type = image.file.content_type.split('/')[0]
    if image_type in settings.CONTENT_TYPES:
        if image.size > settings.MAX_UPLOAD_SIZE:
            raise ValidationError(
                gettext_lazy(
                    'Please keep filesize under {0}. Current filesize {1}').
                format(filesizeformat(settings.MAX_UPLOAD_SIZE),
                       filesizeformat(image.size)))
    else:
        raise ValidationError(gettext_lazy('File type is not supported'))
    return image


class Proof(ext_models.TimeStampedModel):
    user = models.ForeignKey(User, null=True, on_delete=models.SET_NULL)
    image = models.ImageField(upload_to=user_directory_path,
                              validators=[image_size])

// Form submit override code shamelessly stolen from StackOverflow user T.J. Crowder
// https://stackoverflow.com/a/4517530
window.onload = function() {
    document.getElementById('js-warning').style.display = "none";
    var form = document.getElementById('address_form');
    if (form) {
        form.onsubmit = function() {
            var key = document.getElementById('key_field').value;
            if (key.length != 48) {
                document.getElementById('key_field').value = "Set me first!";
                return false
            };
            var txt = document.getElementById('id_address_info');
            txt.value = encrypt(txt.value, key);
        };
    };
};
// Cipher code heavily inspired by StackOverflow user Jorgeblom
// https://stackoverflow.com/a/54026460
let cipher = salt => {
    let textToChars = text => text.split('').map(c => c.charCodeAt(0))
    let S = salt.match(/.{1,4}/g).map(c => parseInt(c,16))
    let bitshift = arr => {
        let ix = (arr, i) => i<0? arr.length+i : i
        var i;
        arr = arr.map(v => ((v & 15) << 12) | (v >> 4))
        var x = arr[ix(arr,-1)];
        for (i=0; i<arr.length-1; i++) {
            arr[ix(arr,i-1)] = (( arr[i] & 65280) >> 8) | (( arr[ix(arr,i-1)] & 255) << 8);
        }
        arr[ix(arr,-2)] = (( x & 65280) >> 8) | (( arr[ix(arr,-2)] & 255) << 8);
        return arr
    }
    let byteHex = n => ("000" + Number(n).toString(16)).substr(-4)
    let applySaltToChar = code => S.reduce((a,b) => a ^ b, code)    

    return text => bitshift(text.split('')
        .map(textToChars))
        .map(applySaltToChar)
        .map(byteHex)
        .join('')
}
let decipher = salt => {
    let S = salt.match(/.{1,4}/g).map(c => parseInt(c,16))
    let applySaltToChar = code => S.reduce((a,b) => a ^ b, code)
    let bitshift = arr => {
        let ix = (arr, i) => i==arr.length? 0 : i
        var i;
        var x = arr[0];
        for (i=arr.length-1; i>0; i--) {
            arr[ix(arr,i+1)] = ((arr[i] & 255) << 8) | ((arr[ix(arr,i+1)] & 65280) >> 8);
        }
        arr[1] = ((x & 255) << 8) | ((arr[1] & 65280) >> 8);
        arr = arr.map(v => ((v & 61440) >> 12) | ((v & 4095) << 4));
        return arr
    }
    return encoded => bitshift(encoded.match(/.{1,4}/g)
        .map(hex => parseInt(hex, 16))
        .map(applySaltToChar))
        .map(charCode => String.fromCharCode(charCode))
        .join('')
}

// Key generation code heavily inspired by StackOverflow user user633183
// https://stackoverflow.com/a/27747377
function dec2hex (dec) {
  return ('000' + dec.toString(16)).substr(-4)
}
function generateId (len) {
  var arr = new Uint16Array((len || 40) / 4)
  window.crypto.getRandomValues(arr)
  return Array.from(arr, dec2hex).join('')
}

// Wrappers
function encrypt (text, key) {
    var ciph = cipher(key);
    return ciph(text)
}

function decrypt () {
    var key = document.getElementById("decrypt_key_in").value;
    if (key.length != 48) {
        document.getElementById("decrypted_data").innerHTML = "<span class='warning'>Key is invalid</span>";
        return false
    }
    var encrypted = document.getElementById("encrypted_data").innerHTML;
    var deciph = decipher(key);
    var decrypted = deciph(encrypted).split("\n");
    var formatPreserve = "<p>";
    for (line in decrypted) {
        // Sanitize the data by nullifying all HTML
        var newl = decrypted[line];
        var oldl;
        do {
            oldl = newl;
            newl = newl.replace("<","&lt;").replace(">","&gt;")
        } while (newl !== oldl);
        formatPreserve = formatPreserve + ""+newl+"<br/>"
    }
    document.getElementById("decrypted_data").innerHTML = formatPreserve+"</p>";
}
function genKey () {
    var key = generateId(48);
    var kf = document.getElementById('key_field');
    kf.value = key;
    kf.select();
    document.execCommand("copy");
    alert("Key was copied to clipboard.");
}

function toggleEncData () {
    var data = document.getElementById('encrypted_data');
    var me = document.getElementById('toggData');
    if (data.style.visibility == "hidden") {
        data.style.visibility = "visible";
        me.innerHTML = "Hide encrypted data";
    } else {
        data.style.visibility = "hidden";
        me.innerHTML = "Show encrypted data";
    }
}

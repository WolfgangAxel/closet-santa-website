# Closet Santa Website

This is the repository for [/r/ClosetSanta](https://www.reddit.com/r/ClosetSanta)'s helper website. It is currently in beta testing.

## Deploying

Here is a (currently untested) script for the initial configuration of the server on a Debian-based machine:

    #! /bin/bash
    RETDIR=$(pwd)
    apt update
    apt install apache2 libapache2-mod-wsgi-py3 libapache2-mod-xsendfile git python3 python3-pip python3-certbot-apache letsencrypt
    a2enmod wsgi
    a2enmod rewrite
    a2enmod xsendfile
    cd /var/www
    git clone https://gitlab.com/WolfgangAxel/closet-santa-website
    cd closet-santa-website
    pip3 install -r requirements.txt
    python3 manage.py makemigrations home users subs admin_extras githealth historicals api
    python3 manage.py migrate
    echo "Create superuser account..."
    python3 manage.py createsuperuser
    echo "Add this server's URL to ALLOWED_HOSTS on line 29... (press enter)"
    read
    #NanoMasterRace
    nano ClosetSanta/settings.py
    mkdir static
    python3 manage.py collectstatic
    python3 manage.py runjob deploy
    cd $RETDIR
    chown -R www-data:www-data /var/www/closet-santa-website
    echo "If you are using Apache, edit 'Templates/Apache_config.conf.template' with your server's information, save it as an available site, enable it, and reload Apache"
    echo "***************"
    echo "The initial configuration is complete. Login to the admin site and do the following:"
    echo "    1. Delete 'example.com' from Sites"
    echo "    2. Add Reddit as a Site"
    echo "    3. Change the SITE_ID setting in 'ClosetSanta/settings.py' to use Reddit's Site ID (reload the server)"
    echo "    4. Create a SocialApplication for Reddit"
    echo "You will also need to schedule cron jobs for hourly/daily events and date events"

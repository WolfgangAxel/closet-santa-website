from django.urls import path
from . import views

urlpatterns = [
    path("all", views.sub_directory),
    path("<sub>", views.load_subreddit),
]

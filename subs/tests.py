from django.test import TestCase, Client
from .models import subreddit, series
from users.models import User


class SubredditTests(TestCase):

    def setUp(self):
        self.c = Client()
        s = series.objects.create(name="weebvision")
        cs = subreddit.objects.create(title="ClosetSanta", related_series=s)
        subreddit.objects.create(title="Animemes", related_series=s)
        User.objects.create(username="Omnipotence_is_bliss",
                            subreddit=cs,
                            accepted_into_exchange=True)
        User.objects.create(username="user1",
                            subreddit=cs,
                            accepted_into_exchange=False)

    def test_1_directory(self):
        r = self.c.get("/r/all")
        self.assertIn(b"ClosetSanta", r.content)
        self.assertIn(b"Animemes", r.content)

    def test_2_only_accepted_users_listed_in_sub(self):
        r1 = self.c.get("/r/ClosetSanta")
        self.assertIn(b"Omnipotence_is_bliss", r1.content)
        self.assertNotIn(b"user1", r1.content)
        r2 = self.c.get("/r/Animemes")
        self.assertNotIn(b"Omnipotence_is_bliss", r2.content)

    def test_3_error_from_nonexistant_sub(self):
        r = self.c.get("/r/anime_irl")
        self.assertIn("error.html", tuple(t.name for t in r.templates))

    def test_4_non_participants_are_hidden(self):
        subreddit.objects.update(participating=False)
        r1 = self.c.get("/r/all")
        self.assertEqual(len(r1.context["subs"]), 0)
        r2 = self.c.get("/r/anime_irl")
        self.assertIn("error.html", tuple(t.name for t in r2.templates))

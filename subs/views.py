from django.shortcuts import render, HttpResponse
from .models import subreddit
from admin_extras.views import task_notifs

# Create your views here.


@task_notifs
def load_subreddit(req, sub):
    try:
        s = subreddit.objects.filter(participating=True).get(title__iexact=sub)
    except:
        ## TODO: Make a proper error page
        context = {
            "title":
            "Sub does not exist",
            "message":
            "Subreddit {} not found".format(sub),
            "reason":
            "This subreddit has not signed up to participate in the exchange."
        }
        return render(req, 'error.html', context)
    table = sorted(s.user_set.filter(accepted_into_exchange__exact=1).all(),
                   key=lambda x: x.username)
    context = {
        'subreddit': str(s),
        'table': table,
    }
    return render(req, 'subs/sub_template.html', context)


@task_notifs
def sub_directory(req):
    subs = sorted(subreddit.objects.filter(participating=True).all(),
                  key=lambda x: x.title.lower())
    return render(req, "subs/directory.html", {'subs': subs})

from django.contrib import admin
from .models import subreddit, series


class SubredditAdmin(admin.ModelAdmin):
    list_display = (
        "title",
        "participating",
        "related_series",
    )


admin.site.register(subreddit, SubredditAdmin)
admin.site.register(series)

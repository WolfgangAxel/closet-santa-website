from django.db import models


class series(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class subreddit(models.Model):
    title = models.CharField(max_length=21)
    related_series = models.ForeignKey(series,
                                       null=False,
                                       on_delete=models.CASCADE)
    participating = models.BooleanField(default=True)

    def __str__(self):
        return "/r/" + self.title

from django.shortcuts import render
from django.conf import settings
from admin_extras.views import task_notifs
from git import Repo
from time import strftime


@task_notifs
def status_view(req):
    repo = Repo(settings.BASE_DIR)
    context = {
        "remote":
        repo.remote().urls.__next__(),
        "commit":
        repo.head.object.name_rev,
        "hash":
        repo.head.object.hexsha,
        "message":
        repo.head.object.message.strip(),
        "dirty":
        "Yes" if repo.is_dirty(untracked_files=True) else "No",
        "author":
        repo.head.object.author.email,
        "generated":
        strftime("%c %Z (%z)"),
        "untracked":
        sorted([d.a_path for d in repo.head.commit.diff(None)] +
               [u + " (new)" for u in repo.untracked_files])
    }
    return render(req, "githealth/git_verify.html", context)

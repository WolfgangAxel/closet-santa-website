from django.test import TestCase, Client
from django.conf import settings
from git import Repo
from shlex import split
from os.path import join


class GitTests(TestCase):

    def setUp(self):
        self.c = Client()
        self.repo = Repo(settings.BASE_DIR)

    def test_1_repo_works(self):
        # We assume gitpython will give correct values
        # This is just to test that the module structure
        # hasn't changed
        _ = self.repo.remote().urls.__next__()
        _ = self.repo.head.object.name_rev
        _ = self.repo.head.object.hexsha
        _ = self.repo.head.object.message.strip()
        _ = self.repo.is_dirty(untracked_files=True)
        _ = self.repo.head.object.author.email
        _ = self.repo.head.commit.diff(None)
        _ = self.repo.untracked_files

    def test_2_enpoint_loads(self):
        res = self.c.get("/site-status/")
        self.assertIn("githealth/git_verify.html",
                      [t.name for t in res.templates])
        self.assertIn(bytes(self.repo.head.object.hexsha, "utf8"), res.content)

from django.test import TestCase, Client
from django.contrib.sites.models import Site
from allauth.socialaccount.models import SocialAccount
from .models import Invitation
from users.models import User
from home import PhaseSettings
from time import gmtime


class SponsorshipTests(TestCase):

    def reset(self, user1kwargs=None, user2kwargs=None):
        if user1kwargs == None:
            user1kwargs = self.user1def
        if user2kwargs == None:
            user2kwargs = self.user2def
        User.objects.filter(username="user1").update(**user1kwargs)
        User.objects.filter(username="user2").update(**user2kwargs)
        user1kwargs = None
        user2kwargs = None

    def setUp(self):
        PhaseSettings.switch_to("Signups")
        self.c = Client()
        self.year = gmtime().tm_year
        self.user1def = {
            "accepted_into_exchange": True,
            "sponsorship_invites": 3,
            "karma": 77777
        }
        self.user2def = {
            "accepted_into_exchange": False,
            "checked_karma": True,
            "banned_until_year": self.year
        }
        self.user1 = User.objects.create(username="user1", **self.user1def)
        self.user2 = User.objects.create(username="user2", **self.user2def)
        self.reset()
        self.current_site = Site.objects.get_current()
        self.socialapp = self.current_site.socialapp_set.create(  # nosec
            provider="reddit",
            name="reddit",
            client_id="1234567890",
            secret="0987654321",
        )
        SocialAccount.objects.create(user=self.user1,
                                     provider="reddit",
                                     uid="user1",
                                     extra_data={"created_utc": 7})

    def test_1_sponsorship_works(self):
        self.reset()
        self.c.force_login(self.user1)
        res = self.c.post("/sponsorships/create/", {
            "username": "user2",
            "acknowledgement": True
        },
                          follow=True)
        self.assertIn(b"user2 was successfully sponsored", res.content)
        self.assertIn("sponsorship/main.html", [t.name for t in res.templates])
        self.assertEqual(Invitation.objects.count(), 1)
        self.c.logout()

    def test_2_already_accepted_fails(self):
        self.reset(
            user2kwargs={
                "accepted_into_exchange": True,
                "checked_karma": True,
                "banned_until_year": self.year,
            })
        self.c.force_login(self.user1)
        res = self.c.post("/sponsorships/create/", {
            "username": "user2",
            "acknowledgement": True
        },
                          follow=True)
        self.assertIn(
            b"The user /u/user2 was not found or is not able to be sponsored.",
            res.content)
        self.c.logout()

    def test_3_karma_not_yet_checked_fails(self):
        self.reset(
            user2kwargs={
                "accepted_into_exchange": False,
                "checked_karma": False,
                "banned_until_year": self.year,
            })
        self.c.force_login(self.user1)
        res = self.c.post("/sponsorships/create/", {
            "username": "user2",
            "acknowledgement": True
        },
                          follow=True)
        self.assertIn(
            b"The user /u/user2 was not found or is not able to be sponsored.",
            res.content)
        self.c.logout()

    def test_4_banned_fails(self):
        self.reset(
            user2kwargs={
                "accepted_into_exchange": False,
                "checked_karma": True,
                "banned_until_year": self.year + 1,
            })
        self.c.force_login(self.user1)
        res = self.c.post("/sponsorships/create/", {
            "username": "user2",
            "acknowledgement": True
        },
                          follow=True)
        self.assertIn(
            b"The user /u/user2 was not found or is not able to be sponsored.",
            res.content)
        self.c.logout()

    def test_5_no_signups_fails(self):
        self.reset()
        PhaseSettings.switch_to("First shipment")
        self.c.force_login(self.user1)
        res = self.c.post("/sponsorships/create/", {
            "username": "user2",
            "acknowledgement": True
        },
                          follow=True)
        self.assertIn(b"Sponsorships cannot be submitted currently.",
                      res.content)
        self.c.logout()

    def test_6_authorized_has_extras(self):
        self.reset()
        res1 = self.c.get("/sponsorships/")
        self.assertFalse(res1.context["show_extra"])
        self.c.force_login(self.user1)
        res2 = self.c.get("/sponsorships/")
        self.assertTrue(res2.context["show_extra"])
        self.c.logout()

    def test_7_form_if_invitations_available(self):
        self.reset()
        self.c.force_login(self.user1)
        res1 = self.c.get("/sponsorships/")
        self.assertIsNotNone(res1.context.get("invitation"))
        self.assertIn(b"I have read the above rules", res1.content)
        self.user1.sponsorship_invites = 0
        self.user1.save()
        res2 = self.c.get("/sponsorships/")
        self.assertIsNone(res2.context.get("invitation"))
        self.c.logout()

    def test_8_sponsored_users_listed(self):
        self.reset()
        self.c.force_login(self.user1)
        res1 = self.c.get("/sponsorships/")
        self.assertNotIn(b"user2", res1.content)
        res2 = self.c.post("/sponsorships/create/", {
            "username": "user2",
            "acknowledgement": True
        },
                           follow=True)
        self.assertIn(b"user2", res2.content)
        self.c.logout()

    def test_9_form_only_during_signups(self):
        # This was the 100th test written 🥳
        # 👢
        # 🐞
        self.reset()
        PhaseSettings.switch_to("First shipment")
        self.c.force_login(self.user1)
        res = self.c.get("/sponsorships/")
        self.assertIsNone(res.context.get("invitation"))
        self.c.logout()

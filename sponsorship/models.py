from django.db import models
from django_extensions.db import models as ext_models
from users.models import User


class Invitation(ext_models.TimeStampedModel):
    sponsor = models.ForeignKey(User,
                                related_name="sponsor_invites",
                                on_delete=models.CASCADE)
    invitee = models.ForeignKey(User,
                                related_name="sponsorships",
                                on_delete=models.CASCADE)
    notified = models.BooleanField(default=False, db_index=True)

    def __str__(self):
        return f"{self.sponsor} -> {self.invitee}"

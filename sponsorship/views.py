from django.shortcuts import render, redirect
from django.contrib import messages
from admin_extras.views import task_notifs
from .models import Invitation
from .forms import InvitationForm
from users.models import User
from users.views import get_context
from time import gmtime
from home import PhaseSettings


@task_notifs
def overview(req):
    context = {
        "show_extra":
        req.user.is_authenticated and get_context(req.user)["pass"] == "Yes"
    }
    if context["show_extra"]:
        context["invites_remaining"] = req.user.sponsorship_invites
        context["invitees"] = tuple(
            invite.invitee for invite in req.user.sponsor_invites.all())
        context[
            "show_form"] = req.user.sponsorship_invites > 0 and PhaseSettings.get_signups_allowed(
            )
        if context["show_form"]:
            context["invitation"] = InvitationForm()
    return render(req, "sponsorship/main.html", context=context)


def create_invitation(req):
    if req.method != "POST":
        messages.add_message(req, messages.ERROR,
                             "Endpoint is only for POST requests.")
        return redirect("/sponsorships/")
    if req.user.is_authenticated == False:
        messages.add_message(req, messages.ERROR,
                             "You must be logged in to do that.")
        return redirect("/sponsorships/")
    if req.user.sponsorship_invites <= 0:
        messages.add_message(req, messages.ERROR,
                             "You do not have any invitations left to give.")
        return redirect("/sponsorships/")
    if PhaseSettings.get_signups_allowed() == False:
        messages.add_message(req, messages.ERROR,
                             "Sponsorships cannot be submitted currently.")
        return redirect("/sponsorships/")
    form = InvitationForm(req.POST)
    if not form.is_valid():
        messages.add_message(req, messages.ERROR,
                             "There was an error in the submitted data.")
        return redirect("/sponsorships/")
    username = form.cleaned_data["username"]
    search = User.objects.filter(username__iexact=username,
                                 accepted_into_exchange=False,
                                 is_sponsored=False,
                                 banned_until_year__lte=gmtime().tm_year,
                                 checked_karma=True)
    if search.count() == 1:
        req.user.sponsorship_invites -= 1
        req.user.save()
        invitee = search.first()
        invitee.accepted_into_exchange = True
        invitee.is_sponsored = True
        invitee.save()
        _ = Invitation.objects.create(sponsor=req.user, invitee=invitee)
        messages.add_message(req, messages.SUCCESS,
                             f"/u/{username} was successfully sponsored")
        return redirect("/sponsorships/")
    messages.add_message(
        req, messages.ERROR,
        f"The user /u/{username} was not found or is not able to be sponsored."
    )
    return redirect("/sponsorships/")

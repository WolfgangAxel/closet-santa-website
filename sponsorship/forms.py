from django import forms


class InvitationForm(forms.Form):
    username = forms.CharField(max_length=20,
                               label="User to sponsor (do not include /u/)")
    acknowledgement = forms.BooleanField(
        label=
        "I have read the above rules and understand the potential consequences of sponsorship"
    )

from allauth.account.adapter import DefaultAccountAdapter
from home import PhaseSettings


class adapter(DefaultAccountAdapter):

    def is_open_for_signup(self, request):
        return PhaseSettings.get_signups_allowed()

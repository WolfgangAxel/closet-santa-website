from django.apps import AppConfig


class AdminExtrasConfig(AppConfig):
    name = 'admin_extras'

from django.test import TestCase, Client
from django.conf import settings
from .models import Task
from users.models import User, Proof
from home import PhaseSettings
from os import mkdir, rmdir, listdir, remove
from os.path import join, exists
from io import BytesIO


class TaskViewTests(TestCase):

    def setUp(self):
        self.c = Client()
        self.mod = User.objects.create(username="mod", is_staff=True)

    def test_1_access_control(self):
        res = self.c.get("/admin/extras/tasks/incomplete")
        self.assertIn("error.html", [t.name for t in res.templates])
        self.c.force_login(self.mod)
        res = self.c.get("/admin/extras/tasks/incomplete")
        self.assertIn("admin_extras/tasks.html",
                      [t.name for t in res.templates])
        self.c.logout()

    def test_2_notification_shown(self):
        # Most views in the site will show the message
        Task.objects.create(title="Testydoo", description="wowee")
        self.c.force_login(self.mod)
        res = self.c.get("/")
        self.assertIn(b"There are unresolved admin tasks to complete.",
                      res.content)
        self.c.logout()

    def test_3_proper_list_views(self):
        Task.objects.create(title="Testydoo",
                            description="wowee",
                            completed=True)
        Task.objects.create(title="Toostydee", description="wowza")
        self.c.force_login(self.mod)
        inc = self.c.get("/admin/extras/tasks/incomplete")
        com = self.c.get("/admin/extras/tasks/completed")
        self.c.logout()
        self.assertIn(b"Toostydee", inc.content)
        self.assertIn(b"Testydoo", com.content)
        self.assertNotIn(b"Testydoo", inc.content)
        self.assertNotIn(b"Toostydee", com.content)

    def test_4_details_page_works(self):
        Task.objects.create(title="Testydoo",
                            description="wowee",
                            completed=True,
                            completed_by=self.mod)
        Task.objects.create(title="Toostydee", description="wowza")
        self.c.force_login(self.mod)
        doo = self.c.get("/admin/extras/tasks/task/testydoo")
        dee = self.c.get("/admin/extras/tasks/task/toostydee")
        self.c.logout()
        self.assertIn(b"Testydoo", doo.content)
        self.assertIn(b"wowee", doo.content)
        self.assertIn(b"Completed by", doo.content)
        self.assertIn(b"Toostydee", dee.content)
        self.assertIn(b"wowza", dee.content)
        self.assertIn(b"submit", dee.content)

    def test_5_completion_moves_task(self):
        Task.objects.create(title="Toostydee", description="wowza")
        self.c.force_login(self.mod)
        res = self.c.post("/admin/extras/tasks/task/toostydee", follow=True)
        self.assertIn(b"Task completed!", res.content)
        self.assertEqual(Task.objects.filter(completed=True).count(), 1)

    def test_6_invalid_task_is_error(self):
        self.c.force_login(self.mod)
        res = self.c.get("/admin/extras/tasks/task/tahstydum")
        self.assertIn("admin_extras/task-error.html",
                      [t.name for t in res.templates])


class ProofDeleteTests(TestCase):
    uploads = join(settings.MEDIA_ROOT)
    was_missing = False

    def setUp(self):
        if not exists(self.uploads):
            mkdir(self.uploads)
            self.was_missing = True
        PhaseSettings.initialize()
        PhaseSettings.switch_to("First shipment")
        self.k = User.objects.create(username="kouhai")  # uid = 2
        self.u = User.objects.create(username="test_user",
                                     kouhai=self.k,
                                     is_staff=True,
                                     is_superuser=True)  # uid = 3
        self.image = open(
            join(settings.BASE_DIR, "home", "static", "home", "bg.gif"), "rb")
        self.c = Client()
        self.c.force_login(self.u)
        res = self.c.post("/u/kouhai/form-response-proof/", {
            "image": self.image,
            "done_submitting_proof": False
        },
                          follow=True)
        self.assertTrue(
            Proof.objects.count(), 1
        )  # This can fail most of the tests so make them fail here if needed

    def tearDown(self):
        user_dir = join(self.uploads, "u_test_user")
        if exists(user_dir):
            for file in listdir(user_dir):
                remove(join(user_dir, file))
            rmdir(user_dir)
        if self.was_missing:
            rmdir(self.uploads)

    def test_1_invalid_proof_redirects(self):
        self.c.force_login(self.u)
        res = self.c.get("/admin/extras/proof-delete/2/999", follow=True)
        self.assertEqual(len(res.redirect_chain), 1)
        self.assertIn(b"Proof not found", res.content)

    def test_2_get_views(self):
        self.c.force_login(self.u)
        res = self.c.get("/admin/extras/proof-delete/3/1")
        self.assertIn("admin_extras/proof_delete_conf.html",
                      [t.name for t in res.templates])

    def test_3_proof_gets_deleted(self):
        self.c.force_login(self.u)
        res = self.c.post("/admin/extras/proof-delete/3/1", follow=True)
        self.assertEqual(len(res.redirect_chain), 1)
        self.assertIn(b"File successfully deleted", res.content)
        self.assertEqual(Proof.objects.count(), 0)
        self.assertEqual(len(listdir(join(self.uploads, "u_test_user"))), 0)

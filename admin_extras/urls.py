from django.urls import path, re_path
from . import views

urlpatterns = [
    re_path("^proof-delete/(?P<uid>\d+)/(?P<pid>\d+)$", views.proof_delete),
    path("tasks/<status>", views.admin_tasks),
    path("tasks/task/<slug>", views.view_task),
]

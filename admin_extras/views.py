from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib import messages
from django.core.files.storage import default_storage
from users import models
from .models import Task


def admin_only(func):

    def wrapper(*args, **kwargs):
        req = args[0]
        if not req.user.is_staff:
            return render(req,
                          "error.html",
                          context={
                              "reason":
                              "You do not have permission to view this page"
                          })
        return func(*args, **kwargs)

    return wrapper


def task_notifs(func):
    # For use on view definitions, req will always be passed in at pos 0
    def wrapper(*args, **kwargs):
        req = args[0]
        if req.user.is_staff:
            if len(Task.objects.filter(completed__exact=False).all()) > 0:
                messages.add_message(
                    req, messages.WARNING,
                    "There are unresolved admin tasks to complete.")
        return func(*args, **kwargs)

    return wrapper


@admin_only
def proof_delete(req, uid, pid):
    search = models.Proof.objects.filter(id=pid)
    if search.count() == 0:
        messages.add_message(req, messages.ERROR, "Proof not found")
        return redirect("/admin/users/user/{}/change/".format(uid))
    proof = search.first()
    user = models.User.objects.get(id=uid)
    if req.method == "POST":
        user.proof_set.remove(proof)
        user.total_uploaded -= proof.image.size
        default_storage.delete(proof.image.path)
        proof.delete()
        user.save()
        messages.add_message(req, messages.SUCCESS,
                             "File successfully deleted")
        return redirect("/admin/users/user/{}/change/".format(uid))
    elif req.method == "GET":
        return render(
            req, 'admin_extras/proof_delete_conf.html', {
                'uid': uid,
                'pid': pid,
                'url': "/u/{0}/proof/{1}".format(user.username, pid)
            })


@admin_only
def admin_tasks(req, status):
    i = int(status == "completed")
    # Want a FIFO queue for incomplete tasks, FILO for completed
    #        (oldest first, [::1])            (newest first, [::-1])
    context = {
        "tasks": list(Task.objects.filter(completed=i).all())[::1 - 2 * i],
        "list": status
    }
    return render(req, "admin_extras/tasks.html", context)


@admin_only
def view_task(req, slug):
    try:
        task = Task.objects.get(slug=slug)
    except:
        return render(req, "admin_extras/task-error.html")
    if req.method == "GET":
        context = {'task': task}
        return render(req, "admin_extras/task.html", context)
    elif req.method == "POST":
        task.completed = True
        task.completed_by = req.user
        task.save()
        messages.add_message(req, messages.SUCCESS, "Task completed!")
        return redirect("/admin/extras/tasks/incompleted")

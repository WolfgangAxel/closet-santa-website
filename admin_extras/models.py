from django.db import models
from django_extensions.db import models as ext_models
from users.models import User


class Task(ext_models.TitleSlugDescriptionModel):
    ## From TitleSlugDescriptionModel:
    #title = models.CharField(max_length=255)
    #description = models.TextField(null=True)
    #slug = ext_models.AutoSlugField(populate_from=['title'])
    created = ext_models.CreationDateTimeField()
    completed = models.BooleanField(default=False)
    completed_by = models.ForeignKey(User,
                                     null=True,
                                     on_delete=models.SET_NULL)
